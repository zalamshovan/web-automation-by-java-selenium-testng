package pages.ui.PageClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.ui.BaseClass;

public class HeaderPanel extends BaseClass {

    //Start: Header panel web elements
    @FindBy(className="img-avatar")
    WebElement profileButton;

    @FindBy(xpath="//button[text()='Sign Out']")
    WebElement signOutButton;

    @FindBy(xpath="//a[text()='Your Profile']")
    WebElement yourProfileButton;

    @FindBy(xpath="//header/span[contains(@class, 'facility')]/div")
    WebElement facilityDropdown;

    @FindBy(xpath="//span[contains(@class, 'facility')]/descendant::span")
    WebElement currentFacility;
    // End: Header panel web elements

    /**
     * Method to get the Logged in user's title
     * @return logged in user's title
     */
    public String getUserLoggedInTitle() {
        return profileButton.getAttribute("title");

    }

    /**
     * To sign out
     */
    public void signOut() {
        explicitlyWait(30).until(ExpectedConditions.elementToBeClickable(profileButton));
        profileButton.click();
        explicitlyWait(30).until(ExpectedConditions.elementToBeClickable(signOutButton));
        signOutButton.click();

    }

    /**
     * Navigate to Profile page
     */
    public void yourProfileButtonClick() {
        explicitlyWait(30).until(ExpectedConditions.elementToBeClickable(profileButton));
        profileButton.click();
        explicitlyWait(30).until(ExpectedConditions.elementToBeClickable(yourProfileButton));
        yourProfileButton.click();

    }

    /**
     * Implementing select facility from facility dropdown
     * @param facilityName
     */
    public void selectFacilityFromDropdown(String facilityName) {

        if(!currentFacility.getText().toLowerCase().equals(facilityName.toLowerCase())) {
            try {
                By facilityLocator = By.xpath("//button/div[(translate(normalize-space(text()),'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='" + facilityName.toLowerCase() + "')]");
                facilityDropdown.click();
                scrollVerticallyTillAppears(facilityLocator);
                clickOn(facilityLocator,10, "Facility Selection");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}