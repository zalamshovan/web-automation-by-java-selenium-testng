package pages.ui.PageClasses;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import pages.ui.BaseClass;

public class RemoteViewerPage extends BaseClass {

    // Start: Remote viewer page web locators
    By playButton = By.xpath("//button[@id='PlayBtn']");
    By backButton = By.xpath("//div[@class='btn-graph-back-container']/a");
    By loadingIcon = By.xpath("//p[text()='Loading...']");
    // End: Remote viewer page web locators

    /**
     * Method to verify if user is successfully navigated to the Remote Viewer
     * @param remoteViewerAccess (Yes/No)
     */
    public void verifyRemoteViewerNavigation(String remoteViewerAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        if(remoteViewerAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(isExpectedElementVisible(playButton, 10, "Remote viewer page"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if(remoteViewerAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(playButton, 10, "Remote viewer page"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Reporter.log("Wrong text provided in the CSV file of remote viewer");
            Assert.fail();

        }

    }

    /**
     * method to navigate to studies page from Remote viewer
     * @param remoteViewerAccess (Yes/No)
     */
    public void goBackToStudiesPage(String remoteViewerAccess) {
        if (remoteViewerAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(clickVerify(backButton, "Remote viewer back button"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (remoteViewerAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(backButton, 10, "Remote viewer back button"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Reporter.log("Wrong text provided in the CSV file of remote viewer");
            Assert.fail();

        }

    }

}
