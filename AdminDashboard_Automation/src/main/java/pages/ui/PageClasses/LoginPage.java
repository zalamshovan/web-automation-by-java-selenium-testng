package pages.ui.PageClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import pages.ui.BaseClass;

public class LoginPage extends BaseClass {

    // Start: Login screen web elements
    @FindBy(how = How.ID, using = "email")
    WebElement usernmaeTextBox;

    @FindBy(how = How.ID, using = "password")
    WebElement passwordTextBox;

    @FindBy(how = How.XPATH, using = "//input[@value='Login']")
    WebElement loginButton;
    // End: Login screen web elements

    // Start: Login screen web locators
    By titleText = By.xpath("//div[contains(@class,'container-fluid')]/descendant::h2");

    By loginError = By.xpath("//div[contains(@class,'toast-container')]/descendant::div[@class='body']");

    // End: Login screen web locators

    // Start: Page specific constant variable
    String dashboardPageTitle = "Rendr Dashboard";
    String dashboardPageURL = "/Dashboard";
    // End: Page specific constant variable

    /***
     * Implementing type user name functionality
     * @param userName
     */
    public void typeUserName(String userName){
        usernmaeTextBox.sendKeys(userName);

    }

    /***
     * Implementing type password functionality
     * @param password
     */
    public void typePassword(String password){
        passwordTextBox.sendKeys(password);

    }

    /***
     * Implementing click login button functionality
     */
    public void clickLoginButton(){
        loginButton.click();

    }

    /***
     * Implementing login page appearance verification
     */
    public void verifyLoginPage(){
        Assert.assertTrue(isExpectedElementVisible(titleText,10, "Login Page"));

    }

    /***
     * Implementing login button disappearance verification
     */
    public void loginButtonAppearance(){
        Assert.assertTrue(isNotExpectedElementVisible(titleText,10, "Login button"));

    }

    /**
     * Method to get error message shown fo invalid credentials
     * @return
     */
    public void checkInvalidCredentialsMessage(){
        String actualMessage = getText(loginError,10,"Invalid credentials toast message");
        String expectedMessage = "The user name or password is incorrect.";
        Assert.assertEquals(actualMessage, expectedMessage);

    }

}

