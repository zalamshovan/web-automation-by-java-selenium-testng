package pages.ui.PageClasses.ListPages;

import org.openqa.selenium.By;

public class UserPage extends BaseClassList {

    // Start: User screen locators
    By pageTitle = By.xpath("//h3[text()=' Users']");
    By createButton = By.xpath("//a[text()='Create']");
    By editUserButton = By.xpath("//tr[1]//a[@title='Edit']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    // End: User screen locators

    /**
     * Method to verify if user is successfully navigated to the Users page
     * @param userPageAccess (Yes/No)
     */
    public void verifyUserPageNavigation(String userPageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(userPageAccess, pageTitle, "User page");

    }

    /**
     * Method to verify if the user has access to enter create user page
     * @param createUserAccess (Yes/No)
     */
    public void verifyCreateButtonClick(String createUserAccess){
        verifyCreateEditPageNavigation(createUserAccess, createButton, "Create user button");

    }

    /**
     * Method to verify if the user has access to enter edit user page
     * @param editUserAccess (Yes/No)
     */
    public void verifyEditButtonClick(String editUserAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyCreateEditPageNavigation(editUserAccess, editUserButton, "Edit user button");

    }

}
