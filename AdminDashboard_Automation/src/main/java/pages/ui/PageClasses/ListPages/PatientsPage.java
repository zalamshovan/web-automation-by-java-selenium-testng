package pages.ui.PageClasses.ListPages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.openqa.selenium.WebElement;

import java.util.Collections;
import java.util.List;

public class PatientsPage extends BaseClassList {

    // Start: Patient page web locators
    By pageTitle = By.xpath("//span[text()='Patients']");
    By createButton = By.xpath("//a[text()='Create']");
    By editButton_FirstRow = By.xpath("//tr[1]//span[@class='userlist-action-icons']/a[@title='Edit']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By searchingIcon = By.xpath("//td[text()='Searching...']");
    By searchTextBar = By.xpath("//div[@class='form-group']/descendant::input");
    By clearButton = By.xpath("//button[text()='clear']");
    By nameHedaer = By.xpath("//th[text()='Name']");
    By nameSortAscendingIcon = By.xpath("//th[text()='Name']/i[@class='fa fa-sort-up']");
    By nameSortDescendingIcon = By.xpath("//th[text()='Name']/i[@class='fa fa-sort-down']");
    By patientIDHedaer = By.xpath("//th[text()='Patient ID']");
    By patientIDSortAscendingIcon = By.xpath("//th[text()='Patient ID']/i[@class='fa fa-sort-up']");
    By patientIDDescendingIcon = By.xpath("//th[text()='Patient ID']/i[@class='fa fa-sort-down']");
    By dobHedaer = By.xpath("//th[text()='DOB']");
    By dobSortAscendingIcon = By.xpath("//th[text()='DOB']/i[@class='fa fa-sort-up']");
    By dobSortDescendingIcon = By.xpath("//th[text()='DOB']/i[@class='fa fa-sort-down']");
    // End: Patient page web locators

    //Start: Patient page web elements
    @FindBy(xpath="//tr/th[1]")
    WebElement firstColumn;

    @FindBy(xpath="//tr/th[2]")
    WebElement secondColumn;

    @FindBy(xpath="//tr/th[3]")
    WebElement thirdColumn;

    @FindBy(xpath="//tr/th[4]")
    WebElement fourthColumn;

    @FindBy(xpath="//tr/th[5]")
    WebElement fifthColumn;
    // End: Patient page web elements

    String todaysDate = GetCurrentDate();

    /**
     * Method to verify if user is successfully navigated to the Patients page
     * @param patientPageAccess (Yes/No)
     */
    public void verifyPatientPageNavigation(String patientPageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(patientPageAccess, pageTitle, "Patient page");

    }

    /**
     * Method to verify if the user has access to enter create patient page
     * @param createPatientAccess (Yes/No)
     */
    public void verifyCreateButtonClick(String createPatientAccess){
        verifyCreateEditPageNavigation(createPatientAccess, createButton, "Create patient button");

    }

    /**
     * Method to verify if the user has access to enter edit patient page
     * @param editPatientAccess (Yes/No)
     */
    public void verifyEditButtonClick(String editPatientAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyCreateEditPageNavigation(editPatientAccess, editButton_FirstRow, "Edit patient button");

    }

    /**
     * Method to verify if the has successfully navigated to the patients page(without param)
     */
    public void verifyPatientPageNavigation(){
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Data load");
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 10, "Patient page"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to verify if the column orders are proper (First Column)
     * @param expectedColumnName
     */
    public void verifyFirstColumnName(String expectedColumnName) {
        try {
            String actualColumnName = firstColumn.getText();
            Assert.assertEquals(actualColumnName, expectedColumnName, "First Column");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to verify if the column orders are proper (Second Column)
     * @param expectedColumnName
     */
    public void verifySecondColumnName(String expectedColumnName){
        try {
            String actualColumnName = secondColumn.getText();
            Assert.assertEquals(actualColumnName, expectedColumnName, "Second Column");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to verify if the column orders are proper (Third Column)
     * @param expectedColumnName
     */
    public void verifyThirdColumnName(String expectedColumnName){
        try {
            String actualColumnName = thirdColumn.getText();
            Assert.assertEquals(actualColumnName, expectedColumnName, "Third Column");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to verify if the column orders are proper (Fourth Column)
     * @param expectedColumnName
     */
    public void verifyFourthColumnName(String expectedColumnName){
        try {
            String actualColumnName = fourthColumn.getText();
            Assert.assertEquals(actualColumnName, expectedColumnName, "Fourth Column");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to verify if the column orders are proper (Fifth Column)
     * @param expectedColumnName
     */
    public void verifyFifthColumnName(String expectedColumnName){
        try {
            String actualColumnName = fifthColumn.getText();
            Assert.assertEquals(actualColumnName, expectedColumnName, "Fifth Column");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to navigate to the Create Patient page
     */
    public void goToCreatePatientPage() {
        try {
            isExpectedElementVisible(createButton, 30, "Create Button");
            clickOn(createButton, 30, "Create Button click");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to implement search functionality
     */
    public void searchForPatient(String searchKeyword) {
        try {
            clearText(searchTextBar);
            sendKeys(searchTextBar, searchKeyword, "Search text bar");
            isNotExpectedElementVisible(searchingIcon, 30, "Searching text");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to implement search functionality for DOB
     */
    public void searchForPatient() {
        try {
            sendKeys(searchTextBar, todaysDate, "Search text bar");
            isNotExpectedElementVisible(searchingIcon, 30, "Searching text");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to verify search functionality
     */
    public void verifySearchedPatient(String searchKeyword) {
        if(searchKeyword.contains("_"))
        {
            try {
                Assert.assertTrue(isExpectedElementVisible(By.xpath("//tbody/tr[1]/td[contains(@title,'"+ searchKeyword + "')]"),30, searchKeyword + "Searched result"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                Assert.assertTrue(isExpectedElementVisible(By.xpath("//tbody/tr[1]/td[contains(text(),'"+ searchKeyword + "')]"),30, searchKeyword + "Searched result"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Method to verify search functionality for DOB
     */
    public void verifySearchedPatient() {
        try {
            Assert.assertTrue(isExpectedElementVisible(By.xpath("//tbody/tr[1]/td[contains(text(),'"+ todaysDate + "')]"),30, todaysDate +"Searched result"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to click clear button
     */
    public void clickOnSearchClearButton() {
        try {
            clickOn(clearButton, "Clear button");
            isNotExpectedElementVisible(loadingIcon, 30, "Loading text");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to click edit icon
     * @param patientName
     */
    public void clickOnEditButton(String patientName) {
        try {
            clickOn(By.xpath("//tr/td[contains(@title,'" + patientName + "')]/../descendant::a[@title='Edit']"), "Edit button");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Method to verify initial sort icon
     */
    public void verifyInitialSortIndicationByName() {
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Loading icon");
            Assert.assertTrue(isExpectedElementVisible(nameSortAscendingIcon, 30, "Name sort ascending order"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Method to change Name sort
     */
    public void changeNameSort() {
        try {
            clickOn(nameHedaer, 30, "Name header");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Method to change patient id sort
     */
    public void changePatientIDSort() {
        try {
            clickOn(patientIDHedaer, 30, "Patient ID header");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Method to change DOB sort
     */
    public void changeDOBSort() {
        try {
            clickOn(dobHedaer, 30, "DOB header");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Method to verify solumn list sort
     * @param attributeValue(Name/DOB/Patient ID)
     * @param sortType(Ascending/Descending)
     */
    public void verifySortedData(String attributeValue, String sortType) {

        List<String> columnData =  getDataByColumnName(attributeValue);
        List<String> columnDataRaw = getDataByColumnName(attributeValue);

        if(sortType.equals("Ascending")) {
            Collections.sort(columnData);
        }
        else if(sortType.equals("Descending")){
            Collections.sort(columnData, Collections.reverseOrder());
        }

        try {
            Assert.assertEquals(columnData, columnDataRaw, attributeValue +" "+ sortType + " "+ "did not match");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to navigate to the Edit Patient page of first patient
     */
    public void goToEditPatientPage() {
        try {
            isExpectedElementVisible(editButton_FirstRow, 30, "Edit Button");
            clickOn(editButton_FirstRow, 30, "Edit Button click");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
