package pages.ui.PageClasses.ListPages;

import org.openqa.selenium.By;

public class AmplifierPage extends BaseClassList {

    // Start: Amplifier page web locators
    By pageTitle = By.xpath("//h3[text()='Amplifiers']");
    By createButton = By.xpath("//a[text()='Create']");
    By editButton = By.xpath("//tr[1]//a[@title='Edit']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    // End: Amplifier page web locators

    /**
     * Method to verify if user is successfully navigated to the Amplifiers page
     * @param amplifierPageAccess (yes/no)
     */
    public void verifyAmplifierPageNavigation(String amplifierPageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(amplifierPageAccess, pageTitle, "Amplifier page");

    }

    /**
     * Method to verify if the user has access to enter create amplifier page
     * @param createAmplifierAccess (Yes/No)
     */
    public void verifyCreateButtonClick(String createAmplifierAccess){
        verifyCreateEditPageNavigation(createAmplifierAccess, createButton, "Create amplifier button");

    }

    /**
     * Method to verify if the user has access to enter edit amplifier page
     * @param editAmplifierAccess (Yes/No)
     */
    public void verifyEditButtonClick(String editAmplifierAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyCreateEditPageNavigation(editAmplifierAccess, editButton, "Edit amplifier button");

    }

}
