package pages.ui.PageClasses.ListPages;

import org.openqa.selenium.By;

public class DevicePage extends BaseClassList {

    // Start: Device screen locators
    By pageTitle = By.xpath("//h3[text()=' Devices']");
    By createButton = By.xpath("//a[text()='Create']");
    By editButton = By.xpath("//tr[1]//a[@title='Edit']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    // End: Device screen locators

    /**
     * Method to verify if user is successfully navigated to the Devices page
     * @param devicePageAccess (Yes/No)
     */
    public void verifyDevicesPageNavigation(String devicePageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(devicePageAccess, pageTitle, "Device page");

    }

    /**
     * Method to verify if the user has access to enter create device page
     * @param createDeviceAccess (Yes/No)
     */
    public void verifyCreateButtonClick(String createDeviceAccess){
        verifyCreateEditPageNavigation(createDeviceAccess, createButton, "Create device button");

    }

    /**
     * Method to verify if the user has access to enter edit device page
     * @param editDeviceAccess (Yes/No)
     */
    public void verifyEditButtonClick(String editDeviceAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyCreateEditPageNavigation(editDeviceAccess, editButton, "Edit device button");

    }
}

