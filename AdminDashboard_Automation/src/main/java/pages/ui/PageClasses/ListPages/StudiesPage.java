package pages.ui.PageClasses.ListPages;

import org.openqa.selenium.By;

public class StudiesPage extends BaseClassList {

    // Start: Studies page web locators
    By pageTitle = By.xpath("//h3[text()=' Studies']");
    By openButton = By.xpath("//tr[1]/td[@class = 'actions-column']/a[@title='Open']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    // End: Studies page web locators

    /**
     * Method to verify if user is successfully navigated to the Studies page
     * @param studyPageAccess (Yes/No)
     */
    public void verifyStudiesPageNavigation(String studyPageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(studyPageAccess, pageTitle, "Study page");
    }

    /**
     * Method to verify if the user has access to enter remote viewer page
     * @param openStudyAccess (Yes/No)
     */
    public void verifyOpenStudyButtonClick(String openStudyAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyCreateEditPageNavigation(openStudyAccess, openButton, "Open study button");

    }

}
