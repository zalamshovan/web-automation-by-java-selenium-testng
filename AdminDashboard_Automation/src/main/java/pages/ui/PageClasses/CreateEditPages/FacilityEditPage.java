package pages.ui.PageClasses.CreateEditPages;

import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;
import sun.rmi.runtime.Log;

public class FacilityEditPage extends BaseClassCreateEdit {

    // Start: Facility edit  screen web locators
    By pageTitle = By.xpath("//h3[text()='Edit Facility']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By facilityNameTextBox = By.xpath("//input[@id='Name']");
    By domainNameTextBox = By.xpath("//input[@id='Domain']");
    By studyNotesTextBox = By.xpath("//textarea[@id='StudyNotesTemplate']");
    By saveButton = By.xpath("//input[@type='submit']");
    // End: Facility edit  screen web locators

    // Start: Facility edit page web elements
    @FindBy(id="LocalStorageDays")
    WebElement LocalStorageDaysDropdown;
    // End: Facility edit page web elements

    /**
     * Method to verify if user is successfully navigated to the Edit Facility page
     * @param editFacilityPageAccess (Yes/No)
     */
    public void verifyEditFacilityPageNavigation(String editFacilityPageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(editFacilityPageAccess, pageTitle, "Edit Facility page");

    }

    /***
     * Implementing edit facility functionality
     * @param editFacilityValue
     * @param facilityName
     * @param domainName
     * @param localStorageValue
     * @param noteTemplateValue
     * @param toastMessage
     */
    public void editFacility(String editFacilityValue, String facilityName, String domainName, String localStorageValue, String noteTemplateValue, String toastMessage){
        if (editFacilityValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(facilityNameTextBox, facilityName, "Facility name");
                Logger.info("Facility name edited");
                Reporter.log("<p>Facility name edited</p>");
                insertValueToTextbox(domainNameTextBox, domainName, "Domain name");
                Logger.info("Domain name edited");
                Reporter.log("<p>Domain name edited</p>");
                insertValueToTextbox(studyNotesTextBox, noteTemplateValue, "Study note");
                Logger.info("Study notes template edited");
                Reporter.log("<p>Study notes template edited</p>");
                dropdownByVisibleText(LocalStorageDaysDropdown, localStorageValue);
                Logger.info("Local storage days value selected");
                Reporter.log("<p>Local storage days value selected</p>");
                buttonClickByLocator(saveButton);
                Logger.info("Save button clicked");
                Reporter.log("<p>Save button clicked</p>");
                MatchToastMessageValue(toastMessage);
                Logger.info("Toast message matched");
                Reporter.log("<p>Toast message matched</p>");

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (editFacilityValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Edit Facility page"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Reporter.log("Wrong text provided in the CSV file for edit facility");
            Assert.fail();

        }

    }

    /***
     * Implementing edit facility functionality
     * @param editFacilityValue
     * @param localStorageValue
     * @param noteTemplateValue
     * @param toastMessage
     */
    public void editFacility_FA(String editFacilityValue, String localStorageValue, String noteTemplateValue, String toastMessage){
        if (editFacilityValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(studyNotesTextBox, noteTemplateValue, "Study note");
                Logger.info("Study notes template edited");
                Reporter.log("<p>Study notes template edited</p>");
                dropdownByVisibleText(LocalStorageDaysDropdown, localStorageValue);
                Logger.info("Local storage days value selected");
                Reporter.log("<p>Local storage days value selected</p>");
                buttonClickByLocator(saveButton);
                Logger.info("Save button clicked");
                Reporter.log("<p>Save button clicked</p>");
                MatchToastMessageValue(toastMessage);
                Logger.info("Toast message matched");
                Reporter.log("<p>Toast message matched</p>");

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (editFacilityValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Edit Facility page"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Logger.info("Wrong text provided in the CSV file for edit facility");
            Reporter.log("<p>Wrong text provided in the CSV file for edit facility</P>");
            Assert.fail();

        }

    }

    /**
     * Method to verify if user is successfully navigated to the Edit Facility page with out param
     */
    public void verifyEditFacilityPageNavigation(){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        isExpectedElementVisible(pageTitle, 30, "Edit Facility page");

    }

    /***
     * Implementing type facility name
     * @param facilityName
     */
    public void typeFacilityName(String facilityName) {
        try {
            clearText(facilityNameTextBox);
            sendKeys(facilityNameTextBox, facilityName, "Facility Name text box");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing type facility name
     * @param domainName
     */
    public void typeDomainName(String domainName) {
        try {
            clearText(domainNameTextBox);
            sendKeys(domainNameTextBox, domainName, "Domain Name text box");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing type study notes template
     * @param studyNotesValue
     */
    public void typeStudyNotes(String studyNotesValue) {
        try {
            clearText(studyNotesTextBox);
            sendKeys(studyNotesTextBox, studyNotesValue, "Study notes text box");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing character length verification
     * @param textFieldName
     * @param expectedCharLength
     */
    public void verifyCharacterLengthEditFacility(String textFieldName, int expectedCharLength){
        if(textFieldName.toLowerCase().equals("facility")){
            verifyCharacterLength(facilityNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("domain")){
            verifyCharacterLength(domainNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("notes")){
            verifyCharacterLength(studyNotesTextBox, textFieldName, expectedCharLength);
        }
        else {
            Reporter.log("Wrong field name provided");
            Logger.info("Wrong field name provided");
            Assert.fail();
        }

    }

}