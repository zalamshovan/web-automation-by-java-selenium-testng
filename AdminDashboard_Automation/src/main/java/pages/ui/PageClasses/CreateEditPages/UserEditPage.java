package pages.ui.PageClasses.CreateEditPages;

import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;
import sun.rmi.runtime.Log;

public class UserEditPage extends BaseClassCreateEdit {

    // Start: User edit  screen web locators
    By pageTitle = By.xpath("//h3[text()='Edit User']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By titleTextBox = By.id("Title");
    By firstNameTextBox = By.id("FirstName");
    By middleNameTextBox = By.id("MiddleName");
    By lastNameTextBox = By.id("LastName");
    By suffixTextbox = By.id("Suffix");
    By emailTextBox = By.id("Email");
    By saveButton = By.id("btn_submit");
    // End: User edit  screen web locators

    // Start: User edit page web elements
    @FindBy(id = "RoleId")
    WebElement roleDropdown;
    // End: User edit page web elements

    /**
     * Method to verify if user is successfully navigated to the Edit User page
     * @param editUserPageAccess (Yes/No)
     */
    public void verifyEditUserPageNavigation(String editUserPageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(editUserPageAccess, pageTitle, "Edit user page");

    }

    /***
     * Implementing create user functionality
     * @param createUserValue
     * @param titleValue
     * @param firstNamevalue
     * @param middleNameValue
     * @param lastNameValue
     * @param suffixValue
     * @param emailValue
     * @param roleValue
     * @param toastMessage
     */
    public void editUser(String createUserValue, String titleValue, String firstNamevalue, String middleNameValue, String lastNameValue, String suffixValue,
                           String emailValue, String roleValue, String toastMessage) {
        if (createUserValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(titleTextBox, titleValue, "Title" );
                Logger.info("Title edited");
                Reporter.log("<p>Title typed</p>");
                insertValueToTextbox(firstNameTextBox, firstNamevalue, "First name");
                Logger.info("First name edited");
                Reporter.log("<p>First name edited</p>");
                insertValueToTextbox(middleNameTextBox, middleNameValue, "Middle name");
                Logger.info("Middle name edited");
                Reporter.log("<p>Middle name edited</p>");
                insertValueToTextbox(lastNameTextBox, lastNameValue, "Last name");
                Logger.info("Last name edited");
                Reporter.log("<p>Last name edited</p>");
                insertValueToTextbox(suffixTextbox, suffixValue, "Suffix");
                Logger.info("Suffix edited");
                Reporter.log("<p>Suffix edited</p>");
                insertValueToTextbox(emailTextBox, emailValue, "Email");
                Logger.info("Email edited");
                Reporter.log("<p>Email edited</p>");
                dropdownByVisibleText(roleDropdown, roleValue);
                Logger.info("Role edited");
                Reporter.log("<p>Role edited</p>");
                buttonClickByLocator(saveButton);
                Logger.info("Save button clicked");
                Reporter.log("<p>Save button clicked</p>");
                MatchToastMessageValue(toastMessage);
                Logger.info("Toast message matched");
                Reporter.log("<p>Toast message matched</p>");

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (createUserValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Edit user page"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Logger.info("Wrong text provided in the CSV file for edit user");
            Reporter.log("<p>Wrong text provided in the CSV file for edit user</p>");
            Assert.fail();

        }

    }

}