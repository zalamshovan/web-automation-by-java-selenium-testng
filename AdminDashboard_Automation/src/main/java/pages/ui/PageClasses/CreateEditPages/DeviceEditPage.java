package pages.ui.PageClasses.CreateEditPages;

import helper.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;

public class DeviceEditPage extends BaseClassCreateEdit {

    // Start: Device edit  screen web locators
    By pageTitle = By.xpath("//h3[text()='Edit Device']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By deviceNameTextBox = By.id("Name");
    By partNumberTextBox = By.id("PartNumber");
    By configurationTextBox = By.id("Configuration");
    By serialNumberTextBox = By.id("SerialNumber");
    By saveButton = By.id("btn_submit");
    // End: Device edit  screen web locators

    /**
     * Method to verify if user is successfully navigated to the Edit Device page
     * @param editDevicePageAccess (Yes/No)
     */
    public void verifyEditDevicePageNavigation(String editDevicePageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(editDevicePageAccess, pageTitle, "Edit Device page");

    }

    /***
     * Implementing edit device functionality
     * @param editDeviceValue
     * @param deviceName
     * @param partNumber
     * @param configValue
     * @param serialNumber
     * @param toastMessage
     */
    public void editDevice(String editDeviceValue, String deviceName, String partNumber, String configValue,
                             String serialNumber, String toastMessage) {
        if (editDeviceValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(deviceNameTextBox, deviceName, "Device name" );
                Logger.info("Device name edited");
                Reporter.log("<p>Device name edited</p>");
                insertValueToTextbox(partNumberTextBox, partNumber, "Serial Number");
                Logger.info("Part number typed");
                Reporter.log("<p>Part number edited</p>");
                insertValueToTextbox(configurationTextBox, configValue, "Configuration value");
                Logger.info("Configuration value edited");
                Reporter.log("<p>Configuration value edited</p>");
                insertValueToTextbox(serialNumberTextBox, serialNumber, "Serial Number");
                Logger.info("Serial Number edited");
                Reporter.log("<p>Serial Number edited</p>");
                buttonClickByLocator(saveButton);
                Logger.info("Save button clicked");
                Reporter.log("<p>Save button clicked</p>");
                MatchToastMessageValue(toastMessage);
                Logger.info("Toast message matched");
                Reporter.log("<p>Toast message matched</p>");

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (editDeviceValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Edit device page"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Logger.info("Wrong text provided in the CSV file for edit device");
            Reporter.log("<p>Wrong text provided in the CSV file for edit device</p>");
            Assert.fail();

        }

    }

}
