package pages.ui.PageClasses.CreateEditPages;

import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

public class AmplifierCreatePage extends BaseClassCreateEdit {

    // Start: Amplifier create  screen web locators
    By pageTitle = By.xpath("//h3[text()='Create Amplifier']");
    By ampNameTextBox = By.id("Name");
    By serialNumberTextBox = By.id("SerialNumber");
    By saveButton = By.id("btn_submit");
    // End: Amplifier create  screen web locators

    // Start: Amplifier create page web elements
    @FindBy(id = "AmplifierTypeID")
    WebElement amplifierTypeDropdown;

    @FindBy(id = "FacilityID")
    WebElement facilityNameDropdown;
    // End: Amplifier create page web elements


    /**
     * Method to verify if user is successfully navigated to the Create Amplifier page
     * @param createAmplifierPageAccess (yes/no)
     */
    public void verifyCreateAmplifierPageNavigation(String createAmplifierPageAccess) {
        verifyListPageNavigation(createAmplifierPageAccess, pageTitle, "Create Amplifier page");

    }

    /***
     * Implementing create amplifier functionality
     * @param createAmplifierValue
     * @param ampName
     * @param serialNumber
     * @param ampType
     * @param facilityName
     */
    public void createAmplifier(String createAmplifierValue, String ampName, String serialNumber, String ampType, String facilityName, String toastMessage) {
        if (createAmplifierValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(ampNameTextBox, ampName, "Amplifier name" );
                Logger.info("Amplifier name typed");
                Reporter.log("<p>Amplifier name typed</p>");
                insertValueToTextbox(serialNumberTextBox, serialNumber, "Serial Number");
                Logger.info("Serial number typed");
                Reporter.log("<p>Serial number typed</p>");
                dropdownByVisibleText(amplifierTypeDropdown, ampType);
                Logger.info("Amplifier value selected");
                Reporter.log("<p>Amplifier value selected</p>");
                dropdownByVisibleText(facilityNameDropdown, facilityName);
                Logger.info("Facility selected");
                Reporter.log("<p>Facility selected</p>");
                buttonClickByLocator(saveButton);
                Logger.info("Save button clicked");
                Reporter.log("<p>Save button clicked</p>");
                MatchToastMessageValue(toastMessage);
                Logger.info("Toast message matched");
                Reporter.log("<p>Toast message matched</p>");

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (createAmplifierValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Create amplifier page"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Logger.info("Wrong text provided in the CSV file for create amplifier");
            Reporter.log("<p>Wrong text provided in the CSV file for create amplifier</p>");
            Assert.fail();

        }

    }

}
