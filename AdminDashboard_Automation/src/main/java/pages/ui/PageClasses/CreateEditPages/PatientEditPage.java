package pages.ui.PageClasses.CreateEditPages;

import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

public class PatientEditPage extends BaseClassCreateEdit {

    // Start: Patient edit  screen web locators
    By pageTitle = By.xpath("//h3[text()='Edit Patient']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By firstNameTextBox = By.id("FirstName");
    By middleNameTextBox = By.id("MiddleName");
    By lastNameTextBox = By.id("LastName");
    By dobTextBox = By.xpath("//label[text()='DOB*']/../../descendant::input");
    By ssnTextBox = By.id("SSN");
    By patientIDTextBox = By.id("PatientID");
    By saveButton = By.id("btn_submit");
    // End: Patient edit  screen web locators

    // Start: Patient edit page web elements
    @FindBy(id = "Sex")
    WebElement sexDropdown;
    // End: Patient edit page web elements

    //Getting current date value
    String currentDate = GetCurrentDate();

    /**
     * Method to verify if user is successfully navigated to the Edit Patient page(with param)
     * @param editPatientPageAccess (Yes/No)
     */
    public void verifyEditPatientPageNavigation(String editPatientPageAccess) {
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(editPatientPageAccess, pageTitle, "Edit Patient page");

    }

    /**
     * Method to verify if user is successfully navigated to the Edit Patient page(without param)
     */
    public void verifyEditPatientPageNavigation() {
        try {
            isNotExpectedElementVisible(loadingIcon, 30, "Data load");
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 10, "Edit Patient page"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing type patient's first name
     * @param firstName
     */
    public void typeFirstName(String firstName) {
        try {
            clearText(firstNameTextBox);
            sendKeys(firstNameTextBox, firstName, "First Name text box");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing type patient's middle name
     * @param middleName
     */
    public void typeMiddleName(String middleName) {
        try {
            clearText(middleNameTextBox);
            sendKeys(middleNameTextBox, middleName, "Middle Name text box");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing type patient's last name
     * @param lastName
     */
    public void typeLastName(String lastName) {
        try {
            clearText(lastNameTextBox);
            sendKeys(lastNameTextBox, lastName, "Last Name text box");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing type patient's DOB
     */
    public void typeDOB() {
        try {
            clearText(dobTextBox);
            sendKeys(dobTextBox, currentDate, "DOB text box");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing type patient's SSN
     * @param ssnValue
     */
    public void typeSSN(String ssnValue) {
        try {
            clearText(ssnTextBox);
            sendKeys(ssnTextBox, ssnValue, "SSN text box");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing select sex value
     * @param sexValue
     */
    public void selectSexValueFromDropdown(String sexValue) {
        try {
            dropdownByVisibleText(sexDropdown, sexValue);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing type patient ID
     * @param patientID
     */
    public void typePatientID(String patientID) {
        try {
            clearText(patientIDTextBox);
            sendKeys(patientIDTextBox, patientID, "Patient ID text box");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing save button click functionality
     */
    public void clickSaveButton() {
        try {
            clickOn(saveButton, "Save button");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing edit patient functionality
     * @param editPatientValue
     * @param firstName
     * @param middleName
     * @param lastName
     * @param sexValue
     * @param ssnValue
     * @param patientIDValue
     * @param toastMessage
     */
    public void editPatient(String editPatientValue, String firstName, String middleName, String lastName,
                            String sexValue, String ssnValue, String patientIDValue, String toastMessage) {
        if (editPatientValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(firstNameTextBox, firstName, "First name");
                Logger.info("First name edited");
                Reporter.log("<p>First name edited</p>");
                insertValueToTextbox(middleNameTextBox, middleName, "Middle name");
                Logger.info("Middle name edited");
                Reporter.log("<p>Middle name edited</p>");
                insertValueToTextbox(lastNameTextBox, lastName, "Last name");
                Logger.info("Last name edited");
                Reporter.log("<p>Last name edited</p>");
                dropdownByVisibleText(sexDropdown, sexValue);
                Logger.info("Sex value edited");
                Reporter.log("<p>Sex value edited</p>");
                insertValueToTextbox(ssnTextBox, ssnValue, "SSN value");
                Logger.info("SSN value edited");
                Reporter.log("<p>SSN value edited</p>");
                insertValueToTextbox(patientIDTextBox, patientIDValue, "Patient ID");
                Logger.info("Patient ID edited");
                Reporter.log("<p>Patient ID edited</p>");
                insertValueToTextbox(dobTextBox, currentDate, "DOB");
                Logger.info("DOB edited");
                Reporter.log("<p>DOB edited</p>");
                buttonClickByLocator(saveButton);
                Logger.info("Save button clicked");
                Reporter.log("<p>Save button clicked</p>");
                MatchToastMessageValue(toastMessage);
                Logger.info("Toast message matched");
                Reporter.log("<p>Toast message matched</p>");

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (editPatientValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Edit Patient page"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Logger.info("Wrong text provided in the CSV file for edit patient");
            Reporter.log("<p>Wrong text provided in the CSV file for edit patient</p>");
            Assert.fail();

        }

    }

}
