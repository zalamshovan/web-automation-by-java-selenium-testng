package pages.ui.PageClasses.CreateEditPages;

import helper.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import sun.rmi.runtime.Log;

public class FacilityCreatePage extends BaseClassCreateEdit {

    // Start: Facility Create screen web locators
    By pageTitle = By.xpath("//h3[text()='Create Facility']");
    By facilityNameTextBox = By.xpath("//input[@id='Name']");
    By domainNameTextBox = By.xpath("//input[@id='Domain']");
    By saveButton = By.xpath("//input[@type='submit']");
    // End: Facility Create screen web locators

    // Start: Facility create page web elements
    // End: Facility create page web elements

    /**
     * Method to verify if user is successfully navigated to the Create Facility page
     * @param createFacilityPageAccess (Yes/No)
     */
    public void verifyCreateFacilityPageNavigation(String createFacilityPageAccess){
        verifyListPageNavigation(createFacilityPageAccess, pageTitle, "Create Facility page");

    }

    /***
     * Implementing create facility functionality
     * @param createFacilityValue
     * @param facilityName
     * @param domainName
     * @param toastMessage
     */
    public void createFacility(String createFacilityValue, String facilityName, String domainName, String toastMessage){
        if (createFacilityValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(facilityNameTextBox, facilityName, "Facility name");
                Logger.info("Facility name typed");
                Reporter.log("<p>Facility name typed</p>");
                insertValueToTextbox(domainNameTextBox, domainName, "Domain name");
                Logger.info("Domain name typed");
                Reporter.log("<p>Domain name typed</p>");
                buttonClickByLocator(saveButton);
                Logger.info("Save button clicked");
                Reporter.log("<p>Save button clicked</p>");
                MatchToastMessageValue(toastMessage);
                Logger.info("Toast message matched");
                Reporter.log("<p>Toast message matched</p>");

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (createFacilityValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Create Facility page"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Logger.info("Wrong text provided in the CSV file for create facility");
            Reporter.log("<p>Wrong text provided in the CSV file for create facility</p>");
            Assert.fail();

        }

    }

    /**
     * Method to verify if user is successfully navigated to the Create Facility page without param
     */
    public void verifyCreateFacilityPageNavigation( ){
        try {
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 10, "Create Facility page"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing type facility name
     * @param facilityName
     */
    public void typeFacilityName(String facilityName) {
        try {
            clearText(facilityNameTextBox);
            sendKeys(facilityNameTextBox, facilityName, "Facility Name text box");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing type facility name
     * @param domainName
     */
    public void typeDomainName(String domainName) {
        try {
            clearText(domainNameTextBox);
            sendKeys(domainNameTextBox, domainName, "Domain Name text box");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing save button click functionality
     */
    public void clickSaveButton() {
        try {
            clickOn(saveButton, "Save Button");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing character length verification
     * @param textFieldName
     * @param expectedCharLength
     */
    public void verifyCharacterLengthCreateFacility(String textFieldName, int expectedCharLength){
        if(textFieldName.toLowerCase().equals("facility")){
            verifyCharacterLength(facilityNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("domain")){
            verifyCharacterLength(domainNameTextBox, textFieldName, expectedCharLength);
        }
        else {
            Reporter.log("Wrong field name provided");
            Logger.info("Wrong field name provided");
            Assert.fail();
        }

    }

}
