package pages.ui.PageClasses.CreateEditPages;

import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

public class UserCreatePage extends BaseClassCreateEdit {

    // Start: User create  screen web locators
    By pageTitle = By.xpath("//h3[text()='Create User']");
    By titleTextBox = By.id("Title");
    By firstNameTextBox = By.id("FirstName");
    By middleNameTextBox = By.id("MiddleName");
    By lastNameTextBox = By.id("LastName");
    By suffixTextbox = By.id("Suffix");
    By emailTextBox = By.id("Email");
    By passwordTextBox = By.id("Password");
    By confirmPasswordTextBox = By.id("ConfirmPassword");
    By saveButton = By.id("btn_submit");
    // End: User create  screen web locators

    // Start: User create page web elements
    @FindBy(id = "RoleId")
    WebElement roleDropdown;
    // End: User create page web elements

    /**
     * Method to verify if user is successfully navigated to the Create User page
     * @param createUserPageAccess (Yes/No)
     */
    public void verifyCreateUserPageNavigation(String createUserPageAccess){
        verifyListPageNavigation(createUserPageAccess, pageTitle, "Create user page");

    }

    /***
     * Implementing create user functionality
     * @param createUserValue
     * @param titleValue
     * @param firstNamevalue
     * @param middleNameValue
     * @param lastNameValue
     * @param suffixValue
     * @param emailValue
     * @param passwordValue
     * @param confirmPasswordValue
     * @param roleValue
     * @param toastMessage
     */
    public void createUser(String createUserValue, String titleValue, String firstNamevalue, String middleNameValue, String lastNameValue, String suffixValue,
                             String emailValue, String passwordValue, String confirmPasswordValue, String roleValue, String toastMessage) {
        if (createUserValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(titleTextBox, titleValue, "Title" );
                Logger.info("Title typed");
                Reporter.log("<p>Title typed</p>");
                insertValueToTextbox(firstNameTextBox, firstNamevalue, "First name");
                Logger.info("First name typed");
                Reporter.log("<p>First name typed</p>");
                insertValueToTextbox(middleNameTextBox, middleNameValue, "Middle name");
                Logger.info("Middle name typed");
                Reporter.log("<p>Middle name typed</p>");
                insertValueToTextbox(lastNameTextBox, lastNameValue, "Last name");
                Logger.info("Last name typed");
                Reporter.log("<p>Last name typed</p>");
                insertValueToTextbox(suffixTextbox, suffixValue, "Suffix");
                Logger.info("Suffix typed");
                Reporter.log("<p>Suffix typed</p>");
                insertValueToTextbox(emailTextBox, emailValue, "Email");
                Logger.info("Email typed");
                Reporter.log("<p>Email typed</p>");
                insertValueToTextbox(passwordTextBox, passwordValue, "Password");
                Logger.info("Password typed");
                Reporter.log("<p>Password typed</p>");
                insertValueToTextbox(confirmPasswordTextBox, confirmPasswordValue, "Confirm password");
                Logger.info("Password confirmed");
                Reporter.log("<p>Password confirmed</p>");
                dropdownByVisibleText(roleDropdown, roleValue);
                Logger.info("Role selected");
                Reporter.log("<p>Role selected</p>");
                buttonClickByLocator(saveButton);
                Logger.info("Save button clicked");
                Reporter.log("<p>Save button clicked</p>");
                MatchToastMessageValue(toastMessage);
                Logger.info("Toast message matched");
                Reporter.log("<p>Toast message matched</p>");

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (createUserValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Create user page"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Logger.info("Wrong text provided in the CSV file for create user");
            Reporter.log("<p>Wrong text provided in the CSV file for create user</p>");
            Assert.fail();

        }

    }

}
