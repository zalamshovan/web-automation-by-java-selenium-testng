package pages.ui.PageClasses.CreateEditPages;

import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

public class DeviceCreatePage extends BaseClassCreateEdit {

    // Start: Device create  screen web locators
    By pageTitle = By.xpath("//h3[text()='Create Device']");
    By deviceNameTextBox = By.id("Name");
    By partNumberTextBox = By.id("PartNumber");
    By configurationTextBox = By.id("Configuration");
    By serialNumberTextBox = By.id("SerialNumber");
    By saveButton = By.id("btn_submit");
    // End: Device create  screen web locators

    // Start: Device create page web elements
    @FindBy(id = "FacilityID")
    WebElement facilityNameDropdown;
    // End: Device create page web elements

    /**
     * Method to verify if user is successfully navigated to the Create Device page
     * @param createDevicePageAccess (Yes/No)
     */
    public void verifyCreateDevicePageNavigation(String createDevicePageAccess){
        verifyListPageNavigation(createDevicePageAccess, pageTitle, "Create Device page");

    }

    /***
     * Implementing create device functionality
     * @param createDeviceValue
     * @param deviceName
     * @param partNumber
     * @param configValue
     * @param serialNumber
     * @param facilityName
     * @param toastMessage
     */
    public void createDevice(String createDeviceValue, String deviceName, String partNumber, String configValue,
                                String serialNumber, String facilityName, String toastMessage) {
        if (createDeviceValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(deviceNameTextBox, deviceName, "Device name" );
                Logger.info("Device name typed");
                Reporter.log("<p>Device name typed</p>");
                insertValueToTextbox(partNumberTextBox, partNumber, "Serial Number");
                Logger.info("Part number typed");
                Reporter.log("<p>Part number typed</p>");
                insertValueToTextbox(configurationTextBox, configValue, "Configuration value");
                Logger.info("Configuration value typed");
                Reporter.log("<p>Configuration value typed</p>");
                insertValueToTextbox(serialNumberTextBox, serialNumber, "Serial Number");
                Logger.info("Serial Number typed");
                Reporter.log("<p>Serial Number typed</p>");
                dropdownByVisibleText(facilityNameDropdown, facilityName);
                Logger.info("Facility name selected");
                Reporter.log("<p>Facility name selected</p>");
                buttonClickByLocator(saveButton);
                Logger.info("Save button clicked");
                Reporter.log("<p>Save button clicked</p>");
                MatchToastMessageValue(toastMessage);
                Logger.info("Toast message matched");
                Reporter.log("<p>Toast message matched</p>");

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (createDeviceValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Create device page"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Logger.info("Wrong text provided in the CSV file for create device");
            Reporter.log("<p>Wrong text provided in the CSV file for create device</p>");
            Assert.fail();

        }

    }

}
