package pages.ui.PageClasses.CreateEditPages;

import browserutility.DriverCommand;
import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import pages.ui.BaseClass;

public class BaseClassCreateEdit extends BaseClass {

    DriverCommand driverCommand;

    /**
     * Method to get the default selected drop down list item
     * @param element
     * @return
     */
    public String getSelectedValueOfDropDownList(WebElement element){
        Select select = new Select(driverCommand.getDriver().findElement(By.xpath("//select")));
        WebElement option = select.getFirstSelectedOption();
        return option.getText();
    }

    /***
     * Select from dropdown by visible text.
     * @param element
     * @param VisibleText
     */
    public void dropdownByVisibleText(WebElement element, String VisibleText){
        scrollVerticallyTillAppears(element);
        Select selObj=new Select(element);
        selObj.selectByVisibleText(VisibleText);

    }

    /***
     * Select from dropdown by index Value.
     * @param element
     * @param indexValue
     */
    public void dropdownByIndexValue(WebElement element, int indexValue){
        scrollVerticallyTillAppears(element);
        Select selObj=new Select(element);
        selObj.selectByIndex(indexValue);

    }

    /***
     * Select from dropdown by Value.
     * @param element
     * @param value
     */
    public void dropdownByValue(WebElement element, String value){
        scrollVerticallyTillAppears(element);
        Select selObj=new Select(element);
        selObj.selectByValue(value);

    }

    /***
     * Provide input to a textbox
     * @param locator
     * @param value
     * @param logMessage
     */
    public void insertValueToTextbox(By locator, String value, String logMessage){
        clearTextUsingKeyboard(locator);
        sendKeys(locator, value, logMessage);

    }

    /***
     * Implementing button click functionality
     * @param buttonLocator
     */
    public void buttonClickByLocator(By buttonLocator){
        clickOn(buttonLocator, "Save button");

    }

    /***
     * Implementing the verification of client side message's presence
     * @param message
     */
    public void verifyPresenceOfClientSideMessage(String message){
        Assert.assertTrue(isExpectedElementVisible(By.xpath("//span[contains(text() , '"+ message + "')]"),30, message + " client side message"));

    }

    /***
     * Implementing the verification of client side message's absence
     * @param message
     */
    public void verifyAbsenceOfClientSideMessage(String message){
        Assert.assertTrue(isNotExpectedElementVisible(By.xpath("//span[contains(text() , '"+ message + "')]"),30, message + " client side message"));

    }

    /***
     * Implementing character length verification
     * @param locator
     * @param textFieldName
     * @param expectedCharLength
     */
    public void verifyCharacterLength(By locator, String textFieldName, int expectedCharLength){
        try {
                String actualText = getAttribute(locator,"value",textFieldName + "TextBox");
                Assert.assertEquals(actualText.length(), expectedCharLength);
            }
        catch (Exception e) {
            e.printStackTrace();
            }

        }

}
