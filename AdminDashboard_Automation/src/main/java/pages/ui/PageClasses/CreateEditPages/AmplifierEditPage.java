package pages.ui.PageClasses.CreateEditPages;

import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

public class AmplifierEditPage extends BaseClassCreateEdit {

    // Start: Amplifier edit  screen web locators
    By pageTitle = By.xpath("//h3[text()='Edit Amplifier']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By ampNameTextBox = By.id("Name");
    By serialNumberTextBox = By.id("SerialNumber");
    By saveButton = By.id("btn_submit");
    // End: Amplifier edit  screen web locators

    // Start: Amplifier edit page web elements
    @FindBy(id = "AmplifierTypeID")
    WebElement amplifierTypeDropdown;
    // End: Amplifier edit page web elements

    /**
     * Method to verify if user is successfully navigated to the Edit Amplifier page
     * @param editAmplifierPageAccess (yes/no)
     */
    public void verifyEditAmplifierPageNavigation(String editAmplifierPageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(editAmplifierPageAccess, pageTitle, "Edit Amplifier page");
    }

    /***
     * Implementing edit patient functionality
     * @param editAmplifierValue
     * @param ampName
     * @param serialNumber
     * @param ampType
     */
    public void editAmplifier(String editAmplifierValue, String ampName, String serialNumber, String ampType, String toastMessage) {
        if (editAmplifierValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(ampNameTextBox, ampName, "Amplifier name");
                Logger.info("Amplifier name edited");
                Reporter.log("<p>Amplifier name edited</p>");
                insertValueToTextbox(serialNumberTextBox, serialNumber, "Serial number");
                Logger.info("Serial number edited");
                Reporter.log("<p>Serial number edited</p>");
                dropdownByVisibleText(amplifierTypeDropdown, ampType);
                Logger.info("Amplifier value edited");
                Reporter.log("<p>Amplifier value edited</p>");
                buttonClickByLocator(saveButton);
                Logger.info("Save button clicked");
                Reporter.log("<p>Save button clicked</p>");
                MatchToastMessageValue(toastMessage);
                Logger.info("Toast message matched");
                Reporter.log("<p>Toast message matched</p>");

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (editAmplifierValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Edit amplifier page"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Logger.info("Wrong text provided in the CSV file for edit amplifier");
            Reporter.log("<p>Wrong text provided in the CSV file for edit amplifier</p>");
            Assert.fail();

        }

    }

}

