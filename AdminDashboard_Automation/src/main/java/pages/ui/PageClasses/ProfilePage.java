package pages.ui.PageClasses;

import org.openqa.selenium.By;
import org.testng.Assert;
import pages.ui.BaseClass;

public class ProfilePage extends BaseClass {

    // Start: Profile page web locators
    By profilePic = By.xpath("//div[@id='profile_pic']");
    // End: Profile page web locators

    /***
     * To validate Profile page navigation
     */
    public void verifyProfilePageNavigation(){

        try{
            Assert.assertTrue(isExpectedElementVisible(profilePic, 10,"Edit patient button"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * To get the user role
     * @param userRole
     */
    public void verifyUserRole(String userRole){
        try{
            Assert.assertTrue(isExpectedElementVisible(By.xpath("//div[@id='user_description' and text() = '"+userRole+"']"), 30, "Proper user role"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}