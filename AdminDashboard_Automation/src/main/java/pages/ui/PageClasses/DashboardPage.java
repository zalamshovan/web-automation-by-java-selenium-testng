package pages.ui.PageClasses;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import pages.ui.BaseClass;

public class DashboardPage extends BaseClass {

    // Start: Dashboard web locators
    By dashboardMenuLocator = By.xpath("//li[@class='nav-item']/a[text()='Dashboard']");
    By studiesForReviewCard = By.xpath("//div[@title='Studies For Review']");
    By studiesInProgressCard = By.xpath("//div[@title='Studies In Progress']");
    By totalStudiesCard = By.xpath("//div[@title='Total Studies']");
    By loadingIcon = By.xpath("//*[text()='Loading...']");
    By latestStudiesCard = By.xpath("//div[@class='container row']");
    By latestStudiesTable = By.xpath("//table[@class='table']");
    // End: Dashboard web locators

    /**
     * Method to verify if dashboard is visible
     * @param dashboardAccess (Yes/No)
     */
    public void isDashboardMenuVisible(String dashboardAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        if(dashboardAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(isExpectedElementVisible(dashboardMenuLocator, 10, "Dashboard menu"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if(dashboardAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(dashboardMenuLocator, 10, "Dashboard menu"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Reporter.log("Wrong text provided in the CSV file of dashboard menu");
            Assert.fail();

        }

    }

    /**
     * Method to verify the presence of studies for review card
     * @return
     */
    public boolean isStudiesForReviewCardPresent() {
        return isExpectedElementVisible(studiesForReviewCard, 10, "Studies For Review card");

    }

    /**
     * Method to verify the presence of studies in progress card
     * @return
     */
    public boolean isStudiesInProgressCardPresent() {
        return isExpectedElementVisible(studiesInProgressCard, 10, "Studies in progress card.");

    }


    /**
     * Method to verify the presence of total studies card
     * @return
     */
    public boolean isTotalStudiesCardPresent() {
        return isExpectedElementVisible(totalStudiesCard, 10, "Total studies card");

    }

    /**
     * Method to verify if study info box is visible
     * @param infoBoxAccess (Yes/No)
     */
    public void verifyInfoBox(String infoBoxAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        if(infoBoxAccess.toLowerCase().toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(isExpectedElementVisible(studiesInProgressCard, 10, "Info Box"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if(infoBoxAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(studiesInProgressCard, 10, "Info Box"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Reporter.log("Wrong text provided in the CSV file for Info box");
            Assert.fail();

        }

    }

    /**
     * Method to verify if latest study card and table are visible
     * @param latestStudiesAccess (Yes/No)
     */
    public void verifyLatestStudiesCard(String latestStudiesAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        if(latestStudiesAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(isExpectedElementVisible(latestStudiesCard, 10, "Latest studies card"));
                Assert.assertTrue(isExpectedElementVisible(latestStudiesTable, 10, "Latest studies table"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if(latestStudiesAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(latestStudiesCard, 10, "Latest studies card"));
                Assert.assertTrue(isNotExpectedElementVisible(latestStudiesTable, 10, "Latest studies table"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Reporter.log("Wrong text provided in the CSV file for Latest studies");
            Assert.fail();

        }

    }

    /**
     * Method to verify if the has successfully navigated to the dashboard page
     */
    public void verifyDashboardPageNavigation(){
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Data load");
            Assert.assertTrue(isExpectedElementVisible(dashboardMenuLocator, 10, "Dashboard page"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
