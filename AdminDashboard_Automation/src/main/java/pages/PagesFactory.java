/**
 * Pages factory class implementation.
 * Author: Jony, Date: September 12, 2018
 */


package pages;

import browserutility.Browser;
import org.openqa.selenium.support.PageFactory;
import pages.ui.PageClasses.*;
import pages.ui.PageClasses.CreateEditPages.*;
import pages.ui.PageClasses.ListPages.*;

public final class PagesFactory {

    private static <T> T GetPage(Class<T> className){
        return PageFactory.initElements(Browser.getWebDriver(), className);
    }

    /**
     * Method to initiate login page
     * @return
     */
    public static LoginPage getLoginPage(){
        return GetPage(LoginPage.class);
    }

    /**
     * Method to initiate dashboard page
     * @return
     */
    public static DashboardPage getDashboardPage(){
        return GetPage(DashboardPage.class);
    }

    /**
     * Method to initiate device page
     * @return
     */
    public static DevicePage getDevicePage(){
        return GetPage(DevicePage.class);
    }

    /**
     * Method to initiate device create page
     * @return
     */
    public static DeviceCreatePage getCreateDevicePage(){return GetPage(DeviceCreatePage.class); }

    /**
     * Method to initiate device edit page
     * @return
     */
    public static DeviceEditPage getEditDevicePage(){ return GetPage(DeviceEditPage.class); }

    /**
     * Method to initiate facility page
     * @return
     */
    public static FacilityPage getFacilityPage(){
        return GetPage(FacilityPage.class);
    }

    /**
     * Method to initiate facility create page
     * @return
     */
    public static FacilityCreatePage getFacilityCreatePage(){ return GetPage(FacilityCreatePage.class); }

    /**
     * Method to initiate facility edit page
     * @return
     */
    public static FacilityEditPage getFacilityEditPage(){
        return GetPage(FacilityEditPage.class);
    }

    /**
     * Method to initiate user page
     * @return
     */
    public static UserPage getUserPage(){ return GetPage(UserPage.class); }

    /**
     * Method to initiate user edit page
     * @return
     */
    public static UserEditPage getUserEditPage(){ return GetPage(UserEditPage.class); }

    /**
     * Method to initiate user create page
     * @return
     */
    public static UserCreatePage getUserCreatePage(){ return GetPage(UserCreatePage.class); }

    /**
     * Method to initiate profile page
     * @return
     */
    public static ProfilePage getProfilePage(){ return GetPage(ProfilePage.class); }

    /**
     * Method to initiate menu sidebar page
     * @return
     */
    public static MenuSidebarPage getMenuSideBarPage () { return GetPage(MenuSidebarPage.class); }

    /**
     * Method to initiate patients page
     * @return
     */
    public static PatientsPage getPatientsPage () { return GetPage(PatientsPage.class); }

    /**
     * Method to initiate header panel
     * @return
     */
    public static HeaderPanel getheaderPanel () { return GetPage(HeaderPanel.class); }

    /**
     * Method to initiate studies page
     * @return
     */
    public static StudiesPage getStudiesPage () { return GetPage(StudiesPage.class); }

    /**
     * Method to initiate patient create page
     * @return
     */
    public static PatientCreatePage getPatientCreatePage () { return GetPage(PatientCreatePage.class); }

    /**
     * Method to initiate patient edit page
     * @return
     */
    public static PatientEditPage getPatientEditPage () { return GetPage(PatientEditPage.class); }

    /**
     * Method to initiate remote viewer page
     * @return
     */
    public static RemoteViewerPage getRemoteViewerPage () { return GetPage(RemoteViewerPage.class); }

    /**
     * Method to initiate amplifier page
     * @return
     */
    public static AmplifierPage getAmplifierPage () { return GetPage(AmplifierPage.class); }

    /**
     * Method to initiate amplifier create page
     * @return
     */
    public static AmplifierCreatePage getAmplifierCreatePage () { return GetPage(AmplifierCreatePage.class); }

    /**
     * Method to initiate amplifier edit page
     * @return
     */
    public static AmplifierEditPage getAmplifierEditPage () { return GetPage(AmplifierEditPage.class); }

}
