package testcases.CharacterConstraints;
import dataprovider.DataProviderClass;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.CreateEditPages.PatientCreatePage;
import pages.ui.PageClasses.CreateEditPages.PatientEditPage;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.ListPages.PatientsPage;
import pages.ui.PageClasses.LoginPage;
import pages.ui.PageClasses.MenuSidebarPage;
import testcases.BaseTest;

public class PatientModuleFields extends BaseTest {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String invalidCharacterLength = "testtesttesttesttesttesttesttesttesttesttesttesttesttesttest"; // 60 characters
    String validCharacterLength = "testtesttesttesttesttesttesttesttesttesttesttestte"; //50 characters
    String validCharacterLengthForID = "testtesttesttesttesttesttestte"; //30 characters

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

        System.out.println("TEST ROLE " + userRoleValue);
        System.out.println("TEST EMAIL " + emailValue);
        System.out.println("TEST PASS " + passwordValue);

    }

    LoginPage loginPage;
    DashboardPage dashboardPage;
    MenuSidebarPage menuSidebarPage;
    PatientsPage patientsPage;
    PatientCreatePage patientCreatePage;
    PatientEditPage patientEditPage;

    /***
     * Implementing test case to validate character constrains of create patient page
     */
    @Test(priority = 2)
    public void CreatePatientCharacterConstraintsVerification(){

        Logger.startTestCase("CreatePatient_CharacterConstraints_Starts");
        Logger.info("CreatePatient_CharacterConstraints_Starts");
        Reporter.log("CreatePatient_CharacterConstraints_Starts");
        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyLoginPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        dashboardPage = PagesFactory.getDashboardPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();
        Logger.info("Rendr Neuro Portal Page Loaded");
        Reporter.log("Rendr Neuro Portal Page Loaded");
        loginPage.typeUserName(emailValue);
        loginPage.typePassword(passwordValue);
        Logger.info("User Credentials Given");
        Reporter.log("User Credentials Given");
        loginPage.clickLoginButton();
        Logger.info("Login button clicked");
        Reporter.log("Login button clicked");
        loginPage.loginButtonAppearance();
        Logger.info("Login successful");
        Reporter.log("Login successful");
        dashboardPage.verifyDashboardPageNavigation();
        Logger.info("Successfully navigated to the dashboard");
        Reporter.log("Successfully navigated to the dashboard");
        menuSidebarPage.goToPatientsPage();
        Logger.info("Patient pane clicked");
        Reporter.log("<p>Patient pane clicked</p>");
        patientsPage.verifyPatientPageNavigation();
        Logger.info("Successfully navigated to the patients page");
        Reporter.log("<p>Successfully navigated to the patients page</p>");
        patientsPage.goToCreatePatientPage();
        Logger.info("Create Patient button clicked");
        Reporter.log("<p>Create Patient button clicked</p>");
        patientCreatePage.verifyCreatePatientPageNavigation();
        Logger.info("Successfully navigated to the create patient page");
        Reporter.log("<p>Successfully navigated to the create patient page</p>");
        patientCreatePage.typeFirstName(invalidCharacterLength);
        Logger.info("Invalid first name typed");
        Reporter.log("<p>Invalid first name typed</p>");
        patientCreatePage.typeMiddleName(invalidCharacterLength);
        Logger.info("Invalid middle name typed");
        Reporter.log("<p>Invalid middle name typed</p>");
        patientCreatePage.typeLastName(invalidCharacterLength);
        Logger.info("Invalid last name typed");
        Reporter.log("<p>Invalid last name typed</p>");
        patientCreatePage.typePatientID(invalidCharacterLength);
        Logger.info("Invalid patient ID typed");
        Reporter.log("<p>Invalid patient ID typed</p>");
        patientCreatePage.clickSaveButton();
        Logger.info("Save button clicked");
        Reporter.log("<p>Save button clicked</p>");
        patientCreatePage.verifyPresenceOfClientSideMessage("A first name is required");
        Logger.info("Client side message of first name found");
        Reporter.log("<p>Client side message of first name found</p>");
        patientCreatePage.verifyPresenceOfClientSideMessage("If a middle name is provided, it must be 50 characters or less");
        Logger.info("Client side message of middle name found");
        Reporter.log("<p>Client side message of middle name found</p>");
        patientCreatePage.verifyPresenceOfClientSideMessage("A last name is required");
        Logger.info("Client side message of last name found");
        Reporter.log("<p>Client side message of last name found</p>");
        patientCreatePage.verifyPresenceOfClientSideMessage("If a patient ID is provided, it must be 30 characters or less");
        Logger.info("Client side message of patient id found");
        Reporter.log("<p>Client side message of patient id found</p>");
        patientCreatePage.typeFirstName(validCharacterLength);
        Logger.info("Valid first name typed");
        Reporter.log("<p>Valid first name typed</p>");
        patientCreatePage.typeMiddleName(validCharacterLength);
        Logger.info("Valid middle name typed");
        Reporter.log("<p>Valid middle name typed</p>");
        patientCreatePage.typeLastName(validCharacterLength);
        Logger.info("Valid last name typed");
        Reporter.log("<p>Valid last name typed</p>");
        patientCreatePage.typePatientID(validCharacterLengthForID);
        Logger.info("Valid patient ID typed");
        Reporter.log("<p>Valid patient ID typed</p>");
        patientCreatePage.clickSaveButton();
        Logger.info("Save button clicked");
        Reporter.log("<p>Save button clicked</p>");
        patientCreatePage.verifyAbsenceOfClientSideMessage("A first name is required");
        Logger.info("Client side message of first name does not found");
        Reporter.log("<p>Client side message of first name does not found</p>");
        patientCreatePage.verifyAbsenceOfClientSideMessage("If a middle name is provided, it must be 50 characters or less");
        Logger.info("Client side message of middle name does not found");
        Reporter.log("<p>Client side message of middle name does not found</p>");
        patientCreatePage.verifyAbsenceOfClientSideMessage("A last name is required");
        Logger.info("Client side message of last name does not found");
        Reporter.log("<p>Client side message of last name does not found</p>");
        patientCreatePage.verifyAbsenceOfClientSideMessage("If a patient ID is provided, it must be 30 characters or less");
        Logger.info("Client side message of patient id does not found");
        Reporter.log("<p>Client side message of patient id does not found</p>");
        Logger.endTestCase("CreatePatient_CharacterConstraints_Ends");
        Logger.info("CreatePatient_CharacterConstraints_Ends");
        Reporter.log("CreatePatient_CharacterConstraints_Ends");
    }


    /***
     * Implementing test case to validate character constrains of edit patient page
     */
    @Test(priority = 3)
    public void EditPatientCharacterConstraintsVerification(){

        Logger.startTestCase("EditPatient_CharacterConstraints_Starts");
        Logger.info("EditPatient_CharacterConstraints_Starts");
        Reporter.log("EditPatient_CharacterConstraints_Starts");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientEditPage = PagesFactory.getPatientEditPage();
        menuSidebarPage.goToPatientsPage();
        Logger.info("Patient pane clicked");
        Reporter.log("<p>Patient pane clicked</p>");
        patientsPage.verifyPatientPageNavigation();
        Logger.info("Successfully navigated to the patients page");
        Reporter.log("<p>Successfully navigated to the patients page</p>");
        patientsPage.goToEditPatientPage();
        Logger.info("Edit Patient button clicked");
        Reporter.log("<p>Edit Patient button clicked</p>");
        patientEditPage.verifyEditPatientPageNavigation();
        Logger.info("Successfully navigated to the edit patient page");
        Reporter.log("<p>Successfully navigated to the edit patient page</p>");
        patientEditPage.typeFirstName(invalidCharacterLength);
        Logger.info("Invalid first name typed");
        Reporter.log("<p>Invalid first name typed</p>");
        patientEditPage.typeMiddleName(invalidCharacterLength);
        Logger.info("Invalid middle name typed");
        Reporter.log("<p>Invalid middle name typed</p>");
        patientEditPage.typeLastName(invalidCharacterLength);
        Logger.info("Invalid last name typed");
        Reporter.log("<p>Invalid last name typed</p>");
        patientEditPage.typePatientID(invalidCharacterLength);
        Logger.info("Invalid patient ID typed");
        Reporter.log("<p>Invalid patient ID typed</p>");
        patientEditPage.clickSaveButton();
        Logger.info("Save button clicked");
        Reporter.log("<p>Save button clicked</p>");
        patientEditPage.verifyPresenceOfClientSideMessage("A first name is required");
        Logger.info("Client side message of first name found");
        Reporter.log("<p>Client side message of first name found</p>");
        patientEditPage.verifyPresenceOfClientSideMessage("If a middle name is provided, it must be 50 characters or less");
        Logger.info("Client side message of middle name found");
        Reporter.log("<p>Client side message of middle name found</p>");
        patientEditPage.verifyPresenceOfClientSideMessage("A last name is required");
        Logger.info("Client side message of last name found");
        Reporter.log("<p>Client side message of last name found</p>");
        patientEditPage.verifyPresenceOfClientSideMessage("If a patient ID is provided, it must be 30 characters or less");
        Logger.info("Client side message of patient id found");
        Reporter.log("<p>Client side message of patient id found</p>");
        patientEditPage.typeFirstName(validCharacterLength);
        Logger.info("Valid first name typed");
        Reporter.log("<p>Valid first name typed</p>");
        patientEditPage.typeMiddleName(validCharacterLength);
        Logger.info("Valid middle name typed");
        Reporter.log("<p>Valid middle name typed</p>");
        patientEditPage.typeLastName(validCharacterLength);
        Logger.info("Valid last name typed");
        Reporter.log("<p>Valid last name typed</p>");
        patientEditPage.typePatientID(validCharacterLengthForID);
        Logger.info("Valid patient ID typed");
        Reporter.log("<p>Valid patient ID typed</p>");
        patientEditPage.clickSaveButton();
        Logger.info("Save button clicked");
        Reporter.log("<p>Save button clicked</p>");
        patientEditPage.verifyAbsenceOfClientSideMessage("A first name is required");
        Logger.info("Client side message of first name does not found");
        Reporter.log("<p>Client side message of first name does not found</p>");
        patientEditPage.verifyAbsenceOfClientSideMessage("If a middle name is provided, it must be 50 characters or less");
        Logger.info("Client side message of middle name does not found");
        Reporter.log("<p>Client side message of middle name does not found</p>");
        patientEditPage.verifyAbsenceOfClientSideMessage("A last name is required");
        Logger.info("Client side message of last name does not found");
        Reporter.log("<p>Client side message of last name does not found</p>");
        patientEditPage.verifyAbsenceOfClientSideMessage("If a patient ID is provided, it must be 30 characters or less");
        Logger.info("Client side message of patient id does not found");
        Reporter.log("<p>Client side message of patient id does not found</p>");
        Logger.endTestCase("EditPatient_CharacterConstraints_Ends");
        Logger.info("EditPatient_CharacterConstraints_Ends");
        Reporter.log("EditPatient_CharacterConstraints_Ends");
    }

}
