package testcases.CharacterConstraints;
import dataprovider.DataProviderClass;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.CreateEditPages.FacilityCreatePage;
import pages.ui.PageClasses.CreateEditPages.FacilityEditPage;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.ListPages.FacilityPage;
import pages.ui.PageClasses.LoginPage;
import pages.ui.PageClasses.MenuSidebarPage;
import testcases.BaseTest;

public class FacilityModuleFields extends BaseTest {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String invalidCharacterLength = "testtesttesttesttesttesttesttesttesttesttesttesttesttesttest"; // 60 characters
    String validCharacterLength = "testtesttesttesttesttesttesttesttesttesttesttestte"; //50 characters
    String validCharacterLengthForID = "testtesttesttesttesttesttestte"; //30 characters

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

        System.out.println("TEST ROLE " + userRoleValue);
        System.out.println("TEST EMAIL " + emailValue);
        System.out.println("TEST PASS " + passwordValue);

    }

    LoginPage loginPage;
    DashboardPage dashboardPage;
    MenuSidebarPage menuSidebarPage;
    FacilityPage facilityPage;
    FacilityCreatePage facilityCreatePage;
    FacilityEditPage facilityEditPage;

    /***
     * Implementing test case to validate character constrains of create facility page
     */
    @Test(priority = 2)
    public void CreateFacilityCharacterConstraintsVerification(){

        Logger.startTestCase("CreateFacility_CharacterConstraints_Starts");
        Logger.info("CreateFacility_CharacterConstraints_Starts");
        Reporter.log("CreateFacility_CharacterConstraints_Starts");
        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyLoginPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        dashboardPage = PagesFactory.getDashboardPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityCreatePage = PagesFactory.getFacilityCreatePage();
        Logger.info("Rendr Neuro Portal Page Loaded");
        Reporter.log("Rendr Neuro Portal Page Loaded");
        loginPage.typeUserName(emailValue);
        loginPage.typePassword(passwordValue);
        Logger.info("User Credentials Given");
        Reporter.log("User Credentials Given");
        loginPage.clickLoginButton();
        Logger.info("Login button clicked");
        Reporter.log("Login button clicked");
        loginPage.loginButtonAppearance();
        Logger.info("Login successful");
        Reporter.log("Login successful");
        dashboardPage.verifyDashboardPageNavigation();
        Logger.info("Successfully navigated to the dashboard");
        Reporter.log("Successfully navigated to the dashboard");
        menuSidebarPage.goToFacilitiesPage();
        Logger.info("Facility pane clicked");
        Reporter.log("<p>Facility pane clicked</p>");
        facilityPage.verifyFacilityPageNavigation();
        Logger.info("Successfully navigated to the facility page");
        Reporter.log("<p>Successfully navigated to the facility page</p>");
        facilityPage.goToCreateFacilityPage();
        Logger.info("Create Facility button clicked");
        Reporter.log("<p>Create Facility button clicked</p>");
        facilityCreatePage.verifyCreateFacilityPageNavigation();
        Logger.info("Successfully navigated to the create facility page");
        Reporter.log("<p>Successfully navigated to the create facility page</p>");
        facilityCreatePage.typeFacilityName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        Logger.info("Invalid Facility name typed");
        Reporter.log("<p>Invalid facility name typed</p>");
        facilityCreatePage.typeDomainName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        Logger.info("Invalid domain name typed");
        Reporter.log("<p>Invalid domain name typed</p>");
        facilityCreatePage.verifyCharacterLengthCreateFacility("Facility", 200);
        Logger.info("Facility name max length matched");
        Reporter.log("<p>Facility name max length matched</p>");
        facilityCreatePage.verifyCharacterLengthCreateFacility("Domain", 200);
        Logger.info("Domain name max length matched");
        Reporter.log("<p>Domain name max length matched</p>");
        Logger.endTestCase("CreateFacility_CharacterConstraints_Ends");
        Logger.info("CreateFacility_CharacterConstraints_Ends");
        Reporter.log("CreateFacility_CharacterConstraints_Ends");
    }

    /***
     * Implementing test case to validate character constrains of edit facility page
     */
    @Test(priority = 3)
    public void EditFacilityCharacterConstraintsVerification(){

        Logger.startTestCase("EditFacility_CharacterConstraints_Starts");
        Logger.info("EditFacility_CharacterConstraints_Starts");
        Reporter.log("EditFacility_CharacterConstraints_Starts");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityEditPage = PagesFactory.getFacilityEditPage();
        menuSidebarPage.goToFacilitiesPage();
        Logger.info("Facility pane clicked");
        Reporter.log("<p>Facility pane clicked</p>");
        facilityPage.verifyFacilityPageNavigation();
        Logger.info("Successfully navigated to the facility page");
        Reporter.log("<p>Successfully navigated to the facility page</p>");
        facilityPage.goToEditFacilityPage();
        Logger.info("Edit Facility button clicked");
        Reporter.log("<p>Edit Facility button clicked</p>");
        facilityEditPage.verifyEditFacilityPageNavigation();
        Logger.info("Successfully navigated to the edit facility page");
        Reporter.log("<p>Successfully navigated to the edit facility page</p>");
        facilityEditPage.typeFacilityName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        Logger.info("Invalid Facility name typed");
        Reporter.log("<p>Invalid facility name typed</p>");
        facilityEditPage.typeDomainName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        Logger.info("Invalid domain name typed");
        Reporter.log("<p>Invalid domain name typed</p>");
        facilityEditPage.typeStudyNotes(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength );
        Logger.info("Invalid study notes typed");
        Reporter.log("<p>Invalid study notes typed</p>");
        facilityEditPage.verifyCharacterLengthEditFacility("Facility", 200);
        Logger.info("Facility name max length matched");
        Reporter.log("<p>Facility name max length matched</p>");
        facilityEditPage.verifyCharacterLengthEditFacility("Domain", 200);
        Logger.info("Domain name max length matched");
        Reporter.log("<p>Domain name max length matched</p>");
        facilityEditPage.verifyCharacterLengthEditFacility("Notes", 2000);
        Logger.info("Domain name max length matched");
        Reporter.log("<p>Domain name max length matched</p>");
        Logger.endTestCase("EditFacility_CharacterConstraints_Ends");
        Logger.info("EditFacility_CharacterConstraints_Ends");
        Reporter.log("EditFacility_CharacterConstraints_Ends");
    }

}
