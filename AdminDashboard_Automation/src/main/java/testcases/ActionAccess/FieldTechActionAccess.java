package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class FieldTechActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for FieldTech
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getFieldTechCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForFieldTech(String userRole, String email, String password) {
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

        System.out.println("TEST ROLE " + userRoleValue);
        System.out.println("TEST EMAIL " + emailValue);
        System.out.println("TEST PASS " + passwordValue);

    }

    /***
     * Getting data from csv file for FieldTech
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getFieldTechData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForFieldTech(String userRole, String dashboard, String infoBox, String latestStudies,
                                   String patientsList, String createPatient, String editPatient, String createDevice,
                                   String editDevice, String devicesList, String amplifiersList, String createAmplifier,
                                   String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                   String studiesList, String openStudy, String usersList, String createUser, String editUser) {
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for FieldTech
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void fieldTechUser_LoginRoleVerificaiton() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_LoginRoleVerification");
        Reporter.log("<p>ValidatePageAccessForFieldTech_LoginRoleVerification</p>");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);
    }

    /***
     * Implementing patient list authorization for FieldTech
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void fieldTechUser_PatientListAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_PatientListAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_PatientListAction</p>");
        actionAccessScriptForPatientModule_List(patientsListValue);
    }

    /***
     * Implementing patient create action authorization for FieldTech
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void fieldTechUser_PatientCreateAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_PatientCreateAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_PatientCreateAction</p>");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);
    }

    /***
     * Implementing patient edit action authorization for FieldTech
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void fieldTechUser_PatientEditAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_PatientEditAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_PatientEditAction</p>");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);
    }

    /***
     * Implementing facility list authorization for FieldTech
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 7)
    public void fieldTechUser_FacilityListAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_FacilityListAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_FacilityListAction</p>");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);
    }

    /***
     * Implementing facility create action authorization for FieldTech
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 8)
    public void fieldTechUser_FacilityCreateAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_FacilityCreateAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_FacilityCreateAction</p>");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "FT");
    }

    /***
     * Implementing facility edit action authorization for FieldTech
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 9)
    public void fieldTechUser_FacilityEditAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_FacilityEditAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_FacilityEditAction</p>");
        actionAccessScriptForFacilityModule_Edit(facilitiesListValue, editFacilityValue, "FT");
    }

    /***
     * Implementing amplifier list authorization for FieldTech
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 10)
    public void fieldTechUser_AmplifierListAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_AmplifierListAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_AmplifierListAction</p>");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);
    }

    /***
     * Implementing amplifier create action authorization for FieldTech
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 11)
    public void fieldTechUser_AmplifierCreateAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_AmplifierCreateAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_AmplifierCreateAction</p>");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "FT");
    }

    /***
     * Implementing amplifier edit action authorization for FieldTech
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 12)
    public void fieldTechUser_AmplifierEditAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_AmplifierEditAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_AmplifierEditAction</p>");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "FT");
    }

    /***
     * Implementing device list authorization for FieldTech
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 13)
    public void fieldTechUser_DeviceListAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_DeviceListAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_DeviceListAction</p>");
        actionAccessScriptForDeviceModule_List(devicesListValue);
    }

    /***
     * Implementing device create action authorization for FieldTech
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 14)
    public void fieldTechUser_DeviceCreateAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_DeviceCreateAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_DeviceCreateAction</p>");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);
    }

    /***
     * Implementing device edit action authorization for FieldTech
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 15)
    public void fieldTechUser_DeviceEditAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_DeviceEditAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_DeviceEditAction</p>");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);
    }

    /***
     * Implementing user list authorization for FieldTech
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 16)
    public void fieldTechUser_UserListAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_UserListAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_UserListAction</p>");
        actionAccessScriptForUserModule_List(usersListValue);
    }

    /***
     * Implementing user create action authorization for FieldTech
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 17)
    public void fieldTechUser_UserCreateAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_UserCreateAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_UserCreateAction</p>");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "FT");
    }

    /***
     * Implementing user edit action authorization for FieldTech
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 18)
    public void fieldTechUser_UserEditAction() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_UserEditAction");
        Reporter.log("<p>ValidatePageAccessForFieldTech_UserEditAction</p>");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "FT");
    }

}
