package testcases.ActionAccess;

import helper.Logger;
import org.testng.Reporter;
import pages.PagesFactory;
import pages.ui.PageClasses.*;
import pages.ui.PageClasses.CreateEditPages.*;
import pages.ui.PageClasses.ListPages.*;
import testcases.BaseTest;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BaseActionAccess extends BaseTest {

    LoginPage loginPage;
    DashboardPage dashboardPage;
    ProfilePage profilePage;
    MenuSidebarPage menuSidebarPage;
    PatientsPage patientsPage;
    HeaderPanel headerPanel;
    PatientCreatePage patientCreatePage;
    PatientEditPage patientEditPage;
    FacilityPage facilityPage;
    FacilityCreatePage facilityCreatePage;
    FacilityEditPage facilityEditPage;
    AmplifierPage amplifierPage;
    AmplifierCreatePage amplifierCreatepage;
    AmplifierEditPage amplifierEditPage;
    DevicePage devicePage;
    DeviceCreatePage deviceCreatePage;
    DeviceEditPage deviceEditPage;
    UserPage userPage;
    UserCreatePage userCreatePage;
    UserEditPage userEditPage;

    Format f = new SimpleDateFormat("MMddyyyy_hhmmss");
    Format g = new SimpleDateFormat("hhmmss");
    String dateTime = f.format(new Date());
    String time = g.format(new Date());

    /***
     * Implementing test case to validate Login page
     * @param email
     * @param password
     */
    public void actionAccessScriptForLogin(String email, String password)
    {
        loginPage = PagesFactory.getLoginPage();

        loginPage.verifyLoginPage();
        Logger.info("Rendr Neuro Portal Page Loaded");
        Reporter.log("<p>Rendr Neuro Portal Page Loaded</p>");
        loginPage.typeUserName(email);
        loginPage.typePassword(password);
        Logger.info("User Credentials Given");
        Reporter.log("<p>User Credentials Given</p>");
        loginPage.clickLoginButton();
        Logger.info("Login button clicked");
        Reporter.log("<p>Login button clicked</p>");
        loginPage.loginButtonAppearance();
    }

    /***
     * Implementing test case to validate user role
     * @param userRole
     */
    public void actionAccessScriptForUserRole(String userRole)
    {
        dashboardPage = PagesFactory.getDashboardPage();
        headerPanel = PagesFactory.getheaderPanel();
        profilePage = PagesFactory.getProfilePage();

        headerPanel.yourProfileButtonClick();
        Logger.info("Your Profile button clicked");
        Reporter.log("<p>Your Profile button clicked</p>");
        profilePage.verifyProfilePageNavigation();
        Logger.info("Successfully navigated to the profile page");
        Reporter.log("<p>Successfully navigated to the profile page</p>");
        profilePage.verifyUserRole(userRole);
        Logger.info("Support role matched");
        Reporter.log("<p>Support role matched</p>");
    }

    /***
     * Implementing test case to validate patient module access
     * @param patientsList
     */
    public void actionAccessScriptForPatientModule_List(String patientsList)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();

        menuSidebarPage.patientPageVerification(patientsList);
        Logger.info("Patient page pane verified");
        Reporter.log("<p>Patient page pane verified</p>");
        patientsPage.verifyPatientPageNavigation(patientsList);
        Logger.info("Patient page navigation verified");
        Reporter.log("<p>Patient page navigation verified</p>");
    }

    /***
     * Implementing test case to validate patient module access
     * @param patientsList
     * @param createPatient
     */
    public void actionAccessScriptForPatientModule_Create(String patientsList, String createPatient)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();
        patientEditPage = PagesFactory.getPatientEditPage();

        menuSidebarPage.patientPageVerification(patientsList);
        Logger.info("Patient page pane verified");
        Reporter.log("<p>Patient page pane verified</p>");
        patientsPage.verifyPatientPageNavigation(patientsList);
        Logger.info("Patient page navigation verified");
        Reporter.log("<p>Patient page navigation verified</p>");
        patientsPage.verifyCreateButtonClick(createPatient);
        Logger.info("Create Patient button click verified");
        Reporter.log("<p>Create Patient button click verified</p>");
        patientCreatePage.verifyCreatePatientPageNavigation(createPatient);
        Logger.info("Create patient page verified");
        Reporter.log("<p>Create patient page verified</p>");
        patientCreatePage.createPatient(createPatient, dateTime + "FN", "MN", "LN",
                "Male", "159753842", "Test123", "Patient created!");
        Logger.info("Patient Created");
        Reporter.log("<p>patient Created</p>");
    }

    /***
     * Implementing test case to validate patient module access
     * @param patientsList
     * @param editPatient
     */
    public void actionAccessScriptForPatientModule_Edit(String patientsList, String editPatient)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();
        patientEditPage = PagesFactory.getPatientEditPage();

        menuSidebarPage.patientPageVerification(patientsList);
        Logger.info("Patient page pane verified");
        Reporter.log("<p>Patient page pane verified</p>");
        patientsPage.verifyPatientPageNavigation(patientsList);
        Logger.info("Patient page navigation verified");
        Reporter.log("<p>Patient page navigation verified</p>");
        patientsPage.clickEditIconByInstanceName(editPatient, dateTime, "Patients");
        Logger.info("Facility edit icon clicked using patient name");
        Reporter.log("<p>Facility edit icon clicked using patient name</p>");
        patientEditPage.verifyEditPatientPageNavigation(editPatient);
        Logger.info("Successfully navigated to the edit patient page");
        Reporter.log("<p>Successfully navigated to the edit patient page</p>");
        patientEditPage.editPatient(editPatient, dateTime + "FN_Edited", "MN_Edited", "LN_Edited",
                "Female", "260817130", "Test_Edited", "Patient saved!");
        Logger.info("Patient edited");
        Reporter.log("<p>Patient edited</p>");

    }

    /***
     * Implementing test case to validate facility module list access
     * @param facilityList
     */
    public void actionAccessScriptForFacilityModule_List(String facilityList)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();

        menuSidebarPage.facilityPageVerification(facilityList);
        Logger.info("Facility page pane verified");
        Reporter.log("<p>Facility page pane verified</p>");
        facilityPage.verifyFacilityPageNavigation(facilityList);
        Logger.info("Facility page navigation verified");
        Reporter.log("<p>Facility page navigation verified</p>");
    }

    /***
     * Implementing test case to validate facility module create access
     * @param facilityList
     * @param createFacility
     */
    public void actionAccessScriptForFacilityModule_Create(String facilityList, String createFacility, String role)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityCreatePage = PagesFactory.getFacilityCreatePage();

        menuSidebarPage.facilityPageVerification(facilityList);
        Logger.info("Facility page pane verified");
        Reporter.log("<p>Facility page pane verified</p>");
        facilityPage.verifyFacilityPageNavigation(facilityList);
        Logger.info("Facility page navigation verified");
        Reporter.log("<p>Facility page navigation verified</p>");
        facilityPage.verifyCreateButtonClick(createFacility);
        Logger.info("Create facility button click verified");
        Reporter.log("<p>Create Facility button click verified</p>");
        facilityCreatePage.verifyCreateFacilityPageNavigation(createFacility);
        Logger.info("Create facility page verified");
        Reporter.log("<p>Create facility page verified</p>");
        facilityCreatePage.createFacility(createFacility, dateTime + role, dateTime + role, "Facility created!");
        Logger.info("Facility Created");
        Reporter.log("<p>Facility Created</p>");

    }

    /***
     * Implementing test case to validate facility module edit access
     * @param facilityList
     * @param editFacility
     */
    public void actionAccessScriptForFacilityModule_Edit(String facilityList, String editFacility, String role)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityEditPage = PagesFactory.getFacilityEditPage();

        menuSidebarPage.facilityPageVerification(facilityList);
        Logger.info("Facility page pane verified");
        Reporter.log("<p>Facility page pane verified</p>");
        facilityPage.verifyFacilityPageNavigation(facilityList);
        Logger.info("Facility page navigation verified");
        Reporter.log("<p>Facility page navigation verified</p>");
        facilityPage.clickEditIconByInstanceName(editFacility, dateTime, "Facilities");
        Logger.info("Facility edit icon clicked using facility name");
        Reporter.log("<p>Facility edit icon clicked using facility name</p>");
        facilityEditPage.verifyEditFacilityPageNavigation(editFacility);
        Logger.info("Edit facility page verified");
        Reporter.log("<p>Edit facility page verified</p>");
        facilityEditPage.editFacility(editFacility, dateTime + role + "_Edited", dateTime + role + "_Edited", "After 10 days",
                                    dateTime + "Test Note Template", "Facility saved!");
        Logger.info("Facility Edited");
        Reporter.log("<p>Facility Edited</p>");
    }

    /***
     * Implementing test case to validate facility module edit access for FACILITY ADMIN (Only one facility)
     * @param facilityList
     * @param editFacility
     */
    public void actionAccessScriptForFacilityModule_Edit_FA(String facilityList, String editFacility, String role)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityEditPage = PagesFactory.getFacilityEditPage();

        menuSidebarPage.facilityPageVerification(facilityList);
        Logger.info("Facility page pane verified");
        Reporter.log("<p>Facility page pane verified</p>");
        facilityPage.verifyFacilityPageNavigation(facilityList);
        Logger.info("Facility page navigation verified");
        Reporter.log("<p>Facility page navigation verified</p>");
        facilityPage.clickEditIconByInstanceName(editFacility, "MobileMedTek", "Facilities");
        Logger.info("Facility edit icon clicked using facility name");
        Reporter.log("<p>Facility edit icon clicked using facility name</p>");
        facilityEditPage.verifyEditFacilityPageNavigation(editFacility);
        Logger.info("Edit facility page verified");
        Reporter.log("<p>Edit facility page verified</p>");
        facilityEditPage.editFacility_FA(editFacility, "After 10 days", dateTime + role + "Test Note Template", "Facility saved!");
        Logger.info("Facility Edited");
        Reporter.log("<p>Facility Edited</p>");
    }

    /***
     * Implementing test case to validate amplifier module list access
     * @param amplifierList
     */
    public void actionAccessScriptForAmplifierModule_List(String amplifierList) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        amplifierPage = PagesFactory.getAmplifierPage();

        menuSidebarPage.amplifierPageVerification(amplifierList);
        Logger.info("Amplifier page pane verified");
        Reporter.log("<p>Amplifier page pane verified</p>");
        amplifierPage.verifyAmplifierPageNavigation(amplifierList);
        Logger.info("Amplifier page navigation verified");
        Reporter.log("<p>Amplifier page navigation verified</p>");
    }

    /***
     * Implementing test case to validate amplifier module create access
     * @param amplifierList
     * @param createAmplifier
     */
    public void actionAccessScriptForAmplifierModule_Create(String amplifierList, String createAmplifier, String role) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        amplifierPage = PagesFactory.getAmplifierPage();
        amplifierCreatepage = PagesFactory.getAmplifierCreatePage();

        menuSidebarPage.amplifierPageVerification(amplifierList);
        Logger.info("Amplifier page pane verified");
        Reporter.log("<p>Amplifier page pane verified</p>");
        amplifierPage.verifyAmplifierPageNavigation(amplifierList);
        Logger.info("Amplifier page navigation verified");
        Reporter.log("<p>Amplifier page navigation verified</p>");
        amplifierPage.verifyCreateButtonClick(createAmplifier);
        Logger.info("Create Amplifier button click verified");
        Reporter.log("<p>Create Amplifier button click verified</p>");
        amplifierCreatepage.verifyCreateAmplifierPageNavigation(createAmplifier);
        Logger.info("Create Amplifier page verified");
        Reporter.log("<p>Create Amplifier page verified</p>");
        amplifierCreatepage.createAmplifier(createAmplifier, dateTime + role, time + role, "Rendr", "MobileMedTek", "Amplifier created!");
        Logger.info("Amplifier Created");
        Reporter.log("<p>Amplifier Created</p>");

    }

    /***
     * Implementing test case to validate amplifier module edit access
     * @param amplifierList
     * @param editAmplifier
     */
    public void actionAccessScriptForAmplifierModule_Edit(String amplifierList, String editAmplifier, String role) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        amplifierPage = PagesFactory.getAmplifierPage();
        amplifierEditPage = PagesFactory.getAmplifierEditPage();

        menuSidebarPage.amplifierPageVerification(amplifierList);
        Logger.info("Amplifier page pane verified");
        Reporter.log("<p>Amplifier page pane verified</p>");
        amplifierPage.verifyAmplifierPageNavigation(amplifierList);
        Logger.info("Amplifier page navigation verified");
        Reporter.log("<p>Amplifier page navigation verified</p>");
        amplifierPage.clickEditIconByInstanceName(editAmplifier, dateTime, "Amplifiers");
        Logger.info("Amplifier edit icon clicked using Amplifier name");
        Reporter.log("<p>Amplifier edit icon clicked using Amplifier name</p>");
        amplifierEditPage.verifyEditAmplifierPageNavigation(editAmplifier);
        Logger.info("Edit Amplifier page verified");
        Reporter.log("<p>Edit Amplifier page verified</p>");
        amplifierEditPage.editAmplifier(editAmplifier, dateTime + role + "_Edited", time + role + "ED", "Trackit Mk3","Amplifier saved!");
        Logger.info("Amplifier Edited");
        Reporter.log("<p>Amplifier Edited</p>");
    }

    /***
     * Implementing test case to validate device module list access
     * @param deviceList
     */
    public void actionAccessScriptForDeviceModule_List(String deviceList) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        devicePage = PagesFactory.getDevicePage();

        menuSidebarPage.devicePageVerification(deviceList);
        Logger.info("Device page pane verified");
        Reporter.log("<p>Device page pane verified</p>");
        devicePage.verifyDevicesPageNavigation(deviceList);
        Logger.info("Device page navigation verified");
        Reporter.log("<p>Device page navigation verified</p>");
    }

    /***
     * Implementing test case to validate device module create access
     * @param deviceList
     * @param createDevice
     */
    public void actionAccessScriptForDeviceModule_Create(String deviceList, String createDevice) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        devicePage = PagesFactory.getDevicePage();
        deviceCreatePage = PagesFactory.getCreateDevicePage();

        menuSidebarPage.devicePageVerification(deviceList);
        Logger.info("Device page pane verified");
        Reporter.log("<p>Device page pane verified</p>");
        devicePage.verifyDevicesPageNavigation(deviceList);
        Logger.info("Device page navigation verified");
        Reporter.log("<p>Device page navigation verified</p>");
        devicePage.verifyCreateButtonClick(createDevice);
        Logger.info("Create Device button click verified");
        Reporter.log("<p>Create Device button click verified</p>");
        deviceCreatePage.verifyCreateDevicePageNavigation(createDevice);
        Logger.info("Create Device page verified");
        Reporter.log("<p>Create Device page verified</p>");
        deviceCreatePage.createDevice(createDevice, dateTime + "Device Name", "Part Number", "Configuration",
                "Serial Number", "MobileMedTek", "Device Created!");
        Logger.info("Device Created");
        Reporter.log("<p>Device Created</p>");

    }

    /***
     * Implementing test case to validate device module edit access
     * @param deviceList
     * @param editDevice
     */
    public void actionAccessScriptForDeviceModule_Edit(String deviceList, String editDevice) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        devicePage = PagesFactory.getDevicePage();
        deviceEditPage = PagesFactory.getEditDevicePage();

        menuSidebarPage.devicePageVerification(deviceList);
        Logger.info("Device page pane verified");
        Reporter.log("<p>Device page pane verified</p>");
        devicePage.verifyDevicesPageNavigation(deviceList);
        Logger.info("Device page navigation verified");
        Reporter.log("<p>Device page navigation verified</p>");
        devicePage.clickEditIconByInstanceName(editDevice, dateTime, "Devices");
        Logger.info("Device edit icon clicked using Device name");
        Reporter.log("<p>Device edit icon clicked using Device name</p>");
        deviceEditPage.verifyEditDevicePageNavigation(editDevice);
        Logger.info("Edit Device page verified");
        Reporter.log("<p>Edit Device page verified</p>");
        deviceEditPage.editDevice(editDevice, dateTime + "DN_Edited", "PN_Edited", "CNFG_Edited", "SN_Edited", "Device saved!");
        Logger.info("Device Edited");
        Reporter.log("<p>Device Edited</p>");

    }

    /***
     * Implementing test case to validate user module list access
     * @param userList
     */
    public void actionAccessScriptForUserModule_List(String userList) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.userPageVerification(userList);
        Logger.info("User page pane verified");
        Reporter.log("<p>User page pane verified</p>");
        userPage.verifyUserPageNavigation(userList);
        Logger.info("User page navigation verified");
        Reporter.log("<p>User page navigation verified</p>");
    }

    /***
     * Implementing test case to validate user module create access
     * @param userList
     * @param createUser
     */
    public void actionAccessScriptForUserModule_Create(String userList, String createUser, String role) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        menuSidebarPage.userPageVerification(userList);
        Logger.info("User page pane verified");
        Reporter.log("<p>User page pane verified</p>");
        userPage.verifyUserPageNavigation(userList);
        Logger.info("User page navigation verified");
        Reporter.log("<p>User page navigation verified</p>");
        userPage.verifyCreateButtonClick(createUser);
        Logger.info("Create User button click verified");
        Reporter.log("<p>Create User button click verified</p>");
        userCreatePage.verifyCreateUserPageNavigation(createUser);
        Logger.info("Create User page verified");
        Reporter.log("<p>Create User page verified</p>");
        userCreatePage.createUser(createUser, "Title", "First Name", "Middle Name", "Last Name", "Suffix",
                dateTime + role + "@enosisbd.com", "Enosis123", "Enosis123", "ReviewDoctor", "User created!");
        Logger.info("User Created");
        Reporter.log("<p>User Created</p>");

    }

    /***
     * Implementing test case to validate user module edit access
     * @param userList
     * @param editUser
     */
    public void actionAccessScriptForUserModule_Edit(String userList, String editUser, String role) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();

        menuSidebarPage.userPageVerification(userList);
        Logger.info("User page pane verified");
        Reporter.log("<p>User page pane verified</p>");
        userPage.verifyUserPageNavigation(userList);
        Logger.info("User page navigation verified");
        Reporter.log("<p>User page navigation verified</p>");
        userPage.clickEditIconByInstanceName(editUser, dateTime, "Users");
        Logger.info("User edit icon clicked using User name");
        Reporter.log("<p>User edit icon clicked using User name</p>");
        userEditPage.verifyEditUserPageNavigation(editUser);
        Logger.info("Edit User page verified");
        Reporter.log("<p>Edit User page verified</p>");
        userEditPage.editUser(editUser, "Title_Edited", "First Name_Edited", "Middle Name_Edited", "Last Name_Edited",
                              "Suffix_Edited", dateTime + role + "@yahoo.com", "LeadTech", "User saved!");
        Logger.info("User Edited");
        Reporter.log("<p>User Edited</p>");

    }

}
