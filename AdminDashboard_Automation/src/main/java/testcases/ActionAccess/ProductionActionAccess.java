package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class ProductionActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for Production
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getProductionCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForProduction(String userRole, String email, String password) {
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

        System.out.println("TEST ROLE " + userRoleValue);
        System.out.println("TEST EMAIL " + emailValue);
        System.out.println("TEST PASS " + passwordValue);

    }

    /***
     * Getting data from csv file for Production
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getProductionData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForProduction(String userRole, String dashboard, String infoBox, String latestStudies,
                                         String patientsList, String createPatient, String editPatient, String createDevice,
                                         String editDevice, String devicesList, String amplifiersList, String createAmplifier,
                                         String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                         String studiesList, String openStudy, String usersList, String createUser, String editUser) {
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for production
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void productionUser_LoginRoleVerificaiton() {
        Logger.startTestCase("ValidatePageAccessForProduction_LoginRoleVerification");
        Reporter.log("<p>ValidatePageAccessForProduction_LoginRoleVerification</p>");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);
    }

    /***
     * Implementing patient list authorization for production
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void productionUser_PatientListAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_PatientListAction");
        Reporter.log("<p>ValidatePageAccessForProduction_PatientListAction</p>");
        actionAccessScriptForPatientModule_List(patientsListValue);
    }

    /***
     * Implementing patient create action authorization for production
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void productionUser_PatientCreateAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_PatientCreateAction");
        Reporter.log("<p>ValidatePageAccessForProduction_PatientCreateAction</p>");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);
    }

    /***
     * Implementing patient edit action authorization for production
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void productionUser_PatientEditAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_PatientEditAction");
        Reporter.log("<p>ValidatePageAccessForProduction_PatientEditAction</p>");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);
    }

    /***
     * Implementing facility list authorization for production
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 7)
    public void productionUser_FacilityListAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_FacilityListAction");
        Reporter.log("<p>ValidatePageAccessForProduction_FacilityListAction</p>");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);
    }

    /***
     * Implementing facility create action authorization for production
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 8)
    public void productionUser_FacilityCreateAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_FacilityCreateAction");
        Reporter.log("<p>ValidatePageAccessForProduction_FacilityCreateAction</p>");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "PRO");
    }

    /***
     * Implementing facility edit action authorization for production
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 9)
    public void productionUser_FacilityEditAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_FacilityEditAction");
        Reporter.log("<p>ValidatePageAccessForProduction_FacilityEditAction</p>");
        actionAccessScriptForFacilityModule_Edit(facilitiesListValue, editFacilityValue, "PRO");
    }

    /***
     * Implementing amplifier list authorization for production
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 10)
    public void productionUser_AmplifierListAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_AmplifierListAction");
        Reporter.log("<p>ValidatePageAccessForProduction_AmplifierListAction</p>");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);
    }

    /***
     * Implementing amplifier create action authorization for production
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 11)
    public void productionUser_AmplifierCreateAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_AmplifierCreateAction");
        Reporter.log("<p>ValidatePageAccessForProduction_AmplifierCreateAction</p>");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "PRO");
    }

    /***
     * Implementing amplifier edit action authorization for production
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 12)
    public void productionUser_AmplifierEditAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_AmplifierEditAction");
        Reporter.log("<p>ValidatePageAccessForProduction_AmplifierEditAction</p>");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "PRO");
    }

    /***
     * Implementing device list authorization for production
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 13)
    public void productionUser_DeviceListAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_DeviceListAction");
        Reporter.log("<p>ValidatePageAccessForProduction_DeviceListAction</p>");
        actionAccessScriptForDeviceModule_List(devicesListValue);
    }

    /***
     * Implementing device create action authorization for production
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 14)
    public void productionUser_DeviceCreateAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_DeviceCreateAction");
        Reporter.log("<p>ValidatePageAccessForProduction_DeviceCreateAction</p>");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);
    }

    /***
     * Implementing device edit action authorization for production
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 15)
    public void productionUser_DeviceEditAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_DeviceEditAction");
        Reporter.log("<p>ValidatePageAccessForProduction_DeviceEditAction</p>");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);
    }

    /***
     * Implementing user list authorization for production
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 16)
    public void productionUser_UserListAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_UserListAction");
        Reporter.log("<p>ValidatePageAccessForProduction_UserListAction</p>");
        actionAccessScriptForUserModule_List(usersListValue);
    }

    /***
     * Implementing user create action authorization for production
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 17)
    public void productionUser_UserCreateAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_UserCreateAction");
        Reporter.log("<p>ValidatePageAccessForProduction_UserCreateAction</p>");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "PRO");
    }

    /***
     * Implementing user edit action authorization for production
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 18)
    public void productionUser_UserEditAction() {
        Logger.startTestCase("ValidatePageAccessForProduction_UserEditAction");
        Reporter.log("<p>ValidatePageAccessForProduction_UserEditAction</p>");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "PRO");
    }

}
