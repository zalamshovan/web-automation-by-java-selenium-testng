package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class OfficePersonnelActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for OfficePersonnel
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getOfficePersonnelCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForOfficePersonnel(String userRole, String email, String password) {
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

        System.out.println("TEST ROLE " + userRoleValue);
        System.out.println("TEST EMAIL " + emailValue);
        System.out.println("TEST PASS " + passwordValue);

    }

    /***
     * Getting data from csv file for OfficePersonnel
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getOfficePersonnelData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForOfficePersonnel(String userRole, String dashboard, String infoBox, String latestStudies,
                                    String patientsList, String createPatient, String editPatient, String createDevice,
                                    String editDevice, String devicesList, String amplifiersList, String createAmplifier,
                                    String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                    String studiesList, String openStudy, String usersList, String createUser, String editUser) {
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for OfficePersonnel
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void officePersonnelUser_LoginRoleVerificaiton() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_LoginRoleVerification");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_LoginRoleVerification</p>");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);
    }

    /***
     * Implementing patient list authorization for OfficePersonnel
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void officePersonnelUser_PatientListAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_PatientListAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_PatientListAction</p>");
        actionAccessScriptForPatientModule_List(patientsListValue);
    }

    /***
     * Implementing patient create action authorization for OfficePersonnel
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void officePersonnelUser_PatientCreateAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_PatientCreateAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_PatientCreateAction</p>");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);
    }

    /***
     * Implementing patient edit action authorization for OfficePersonnel
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void officePersonnelUser_PatientEditAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_PatientEditAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_PatientEditAction</p>");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);
    }

    /***
     * Implementing facility list authorization for OfficePersonnel
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 7)
    public void officePersonnelUser_FacilityListAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_FacilityListAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_FacilityListAction</p>");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);
    }

    /***
     * Implementing facility create action authorization for OfficePersonnel
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 8)
    public void officePersonnelUser_FacilityCreateAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_FacilityCreateAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_FacilityCreateAction</p>");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "OP");
    }

    /***
     * Implementing facility edit action authorization for OfficePersonnel
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 9)
    public void officePersonnelUser_FacilityEditAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_FacilityEditAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_FacilityEditAction</p>");
        actionAccessScriptForFacilityModule_Edit(facilitiesListValue, editFacilityValue, "OP");
    }

    /***
     * Implementing amplifier list authorization for OfficePersonnel
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 10)
    public void officePersonnelUser_AmplifierListAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_AmplifierListAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_AmplifierListAction</p>");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);
    }

    /***
     * Implementing amplifier create action authorization for OfficePersonnel
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 11)
    public void officePersonnelUser_AmplifierCreateAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_AmplifierCreateAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_AmplifierCreateAction</p>");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "OP");
    }

    /***
     * Implementing amplifier edit action authorization for OfficePersonnel
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 12)
    public void officePersonnelUser_AmplifierEditAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_AmplifierEditAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_AmplifierEditAction</p>");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "OP");
    }

    /***
     * Implementing device list authorization for OfficePersonnel
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 13)
    public void officePersonnelUser_DeviceListAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_DeviceListAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_DeviceListAction</p>");
        actionAccessScriptForDeviceModule_List(devicesListValue);
    }

    /***
     * Implementing device create action authorization for OfficePersonnel
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 14)
    public void officePersonnelUser_DeviceCreateAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_DeviceCreateAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_DeviceCreateAction</p>");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);
    }

    /***
     * Implementing device edit action authorization for OfficePersonnel
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 15)
    public void officePersonnelUser_DeviceEditAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_DeviceEditAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_DeviceEditAction</p>");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);
    }

    /***
     * Implementing user list authorization for OfficePersonnel
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 16)
    public void officePersonnelUser_UserListAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_UserListAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_UserListAction</p>");
        actionAccessScriptForUserModule_List(usersListValue);
    }

    /***
     * Implementing user create action authorization for OfficePersonnel
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 17)
    public void officePersonnelUser_UserCreateAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_UserCreateAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_UserCreateAction</p>");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "OP");
    }

    /***
     * Implementing user edit action authorization for OfficePersonnel
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 18)
    public void officePersonnelUser_UserEditAction() {
        Logger.startTestCase("ValidatePageAccessForOfficePersonnel_UserEditAction");
        Reporter.log("<p>ValidatePageAccessForOfficePersonnel_UserEditAction</p>");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "OP");
    }

}
