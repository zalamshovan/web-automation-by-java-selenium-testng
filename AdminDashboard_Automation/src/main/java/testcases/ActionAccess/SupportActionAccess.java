package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class SupportActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for Support
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSupportCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSupport(String userRole, String email, String password) {
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

        System.out.println("TEST ROLE " + userRoleValue);
        System.out.println("TEST EMAIL " + emailValue);
        System.out.println("TEST PASS " + passwordValue);

    }

    /***
     * Getting data from csv file for support
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getSupportData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForSupport(String userRole, String dashboard, String infoBox, String latestStudies,
                                     String patientsList, String createPatient, String editPatient, String createDevice,
                                     String editDevice, String devicesList, String amplifiersList, String createAmplifier,
                                     String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                     String studiesList, String openStudy, String usersList, String createUser, String editUser) {
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for support
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void supportUser_LoginRoleVerificaiton() {
        Logger.startTestCase("ValidatePageAccessForSupport_LoginRoleVerification");
        Reporter.log("<p>ValidatePageAccessForSupport_LoginRoleVerification</p>");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);
    }

    /***
     * Implementing patient list authorization for support
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void supportUser_PatientListAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_PatientListAction");
        Reporter.log("<p>ValidatePageAccessForSupport_PatientListAction</p>");
        actionAccessScriptForPatientModule_List(patientsListValue);
    }

    /***
     * Implementing patient create action authorization for support
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void supportUser_PatientCreateAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_PatientCreateAction");
        Reporter.log("<p>ValidatePageAccessForSupport_PatientCreateAction</p>");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);
    }

    /***
     * Implementing patient edit action authorization for support
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void supportUser_PatientEditAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_PatientEditAction");
        Reporter.log("<p>ValidatePageAccessForSupport_PatientEditAction</p>");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);
    }

    /***
     * Implementing facility list authorization for support
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 7)
    public void supportUser_FacilityListAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_FacilityListAction");
        Reporter.log("<p>ValidatePageAccessForSupport_FacilityListAction</p>");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);
    }

    /***
     * Implementing facility create action authorization for support
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 8)
    public void supportUser_FacilityCreateAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_FacilityCreateAction");
        Reporter.log("<p>ValidatePageAccessForSupport_FacilityCreateAction</p>");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "SUP");
    }

    /***
     * Implementing facility edit action authorization for support
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 9)
    public void supportUser_FacilityEditAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_FacilityEditAction");
        Reporter.log("<p>ValidatePageAccessForSupport_FacilityEditAction</p>");
        actionAccessScriptForFacilityModule_Edit(facilitiesListValue, editFacilityValue, "SUP");
    }

    /***
     * Implementing amplifier list authorization for support
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 10)
    public void supportUser_AmplifierListAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_AmplifierListAction");
        Reporter.log("<p>ValidatePageAccessForSupport_AmplifierListAction</p>");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);
    }

    /***
     * Implementing amplifier create action authorization for support
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 11)
    public void supportUser_AmplifierCreateAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_AmplifierCreateAction");
        Reporter.log("<p>ValidatePageAccessForSupport_AmplifierCreateAction</p>");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "SUP");
    }

    /***
     * Implementing amplifier edit action authorization for support
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 12)
    public void supportUser_AmplifierEditAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_AmplifierEditAction");
        Reporter.log("<p>ValidatePageAccessForSupport_AmplifierEditAction</p>");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "SUP");
    }

    /***
     * Implementing device list authorization for support
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 13)
    public void supportUser_DeviceListAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_DeviceListAction");
        Reporter.log("<p>ValidatePageAccessForSupport_DeviceListAction</p>");
        actionAccessScriptForDeviceModule_List(devicesListValue);
    }

    /***
     * Implementing device create action authorization for support
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 14)
    public void supportUser_DeviceCreateAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_DeviceCreateAction");
        Reporter.log("<p>ValidatePageAccessForSupport_DeviceCreateAction</p>");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);
    }

    /***
     * Implementing device edit action authorization for support
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 15)
    public void supportUser_DeviceEditAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_DeviceEditAction");
        Reporter.log("<p>ValidatePageAccessForSupport_DeviceEditAction</p>");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);
    }

    /***
     * Implementing user list authorization for support
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 16)
    public void supportUser_UserListAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_UserListAction");
        Reporter.log("<p>ValidatePageAccessForSupport_UserListAction</p>");
        actionAccessScriptForUserModule_List(usersListValue);
    }

    /***
     * Implementing user create action authorization for support
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 17)
    public void supportUser_UserCreateAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_UserCreateAction");
        Reporter.log("<p>ValidatePageAccessForSupport_UserCreateAction</p>");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "SUP");
    }

    /***
     * Implementing user edit action authorization for support
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 18)
    public void supportUser_UserEditAction() {
        Logger.startTestCase("ValidatePageAccessForSupport_UserEditAction");
        Reporter.log("<p>ValidatePageAccessForSupport_UserEditAction</p>");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "SUP");
    }

}
