package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class SuperAdminActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

        System.out.println("TEST ROLE " + userRoleValue);
        System.out.println("TEST EMAIL " + emailValue);
        System.out.println("TEST PASS " + passwordValue);

    }

    /***
     * Getting data from csv file for superAdmin
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getSuperAdminData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForSuperAdmin(String userRole, String dashboard, String infoBox, String latestStudies,
                                     String patientsList, String createPatient, String editPatient, String createDevice,
                                     String editDevice, String devicesList, String amplifiersList, String createAmplifier,
                                     String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                     String studiesList, String openStudy, String usersList, String createUser, String editUser) {
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for superAdmin
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void superAdminUser_LoginRoleVerificaiton() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_LoginRoleVerification");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_LoginRoleVerification</p>");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);
    }

    /***
     * Implementing patient list authorization for superAdmin
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void superAdminUser_PatientListAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_PatientListAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_PatientListAction</p>");
        actionAccessScriptForPatientModule_List(patientsListValue);
    }

    /***
     * Implementing patient create action authorization for superAdmin
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void superAdminUser_PatientCreateAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_PatientCreateAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_PatientCreateAction</p>");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);
    }

    /***
     * Implementing patient edit action authorization for superAdmin
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void superAdminUser_PatientEditAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_PatientEditAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_PatientEditAction</p>");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);
    }

    /***
     * Implementing facility list authorization for superAdmin
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 7)
    public void superAdminUser_FacilityListAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_FacilityListAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_FacilityListAction</p>");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);
    }

    /***
     * Implementing facility create action authorization for superAdmin
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 8)
    public void superAdminUser_FacilityCreateAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_FacilityCreateAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_FacilityCreateAction</p>");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "SA");
    }

    /***
     * Implementing facility edit action authorization for superAdmin
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 9)
    public void superAdminUser_FacilityEditAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_FacilityEditAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_FacilityEditAction</p>");
        actionAccessScriptForFacilityModule_Edit(facilitiesListValue, editFacilityValue, "SA");
    }

    /***
     * Implementing amplifier list authorization for superAdmin
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 10)
    public void superAdminUser_AmplifierListAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_AmplifierListAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_AmplifierListAction</p>");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);
    }

    /***
     * Implementing amplifier create action authorization for superAdmin
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 11)
    public void superAdminUser_AmplifierCreateAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_AmplifierCreateAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_AmplifierCreateAction</p>");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "SA");
    }

    /***
     * Implementing amplifier edit action authorization for superAdmin
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 12)
    public void superAdminUser_AmplifierEditAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_AmplifierEditAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_AmplifierEditAction</p>");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "SA");
    }

    /***
     * Implementing device list authorization for superAdmin
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 13)
    public void superAdminUser_DeviceListAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_DeviceListAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_DeviceListAction</p>");
        actionAccessScriptForDeviceModule_List(devicesListValue);
    }

    /***
     * Implementing device create action authorization for superAdmin
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 14)
    public void superAdminUser_DeviceCreateAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_DeviceCreateAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_DeviceCreateAction</p>");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);
    }

    /***
     * Implementing device edit action authorization for superAdmin
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 15)
    public void superAdminUser_DeviceEditAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_DeviceEditAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_DeviceEditAction</p>");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);
    }

    /***
     * Implementing user list authorization for superAdmin
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 16)
    public void superAdminUser_UserListAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_UserListAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_UserListAction</p>");
        actionAccessScriptForUserModule_List(usersListValue);
    }

    /***
     * Implementing user create action authorization for superAdmin
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 17)
    public void superAdminUser_UserCreateAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_UserCreateAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_UserCreateAction</p>");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "SA");
    }

    /***
     * Implementing user edit action authorization for superAdmin
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 18)
    public void superAdminUser_UserEditAction() {
        Logger.startTestCase("ValidatePageAccessForSuperAdmin_UserEditAction");
        Reporter.log("<p>ValidatePageAccessForSuperAdmin_UserEditAction</p>");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "SA");
    }

}
