package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class ReviewDoctorActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for ReviewDoctor
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getReviewDoctorCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForReviewDoctor(String userRole, String email, String password) {
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

        System.out.println("TEST ROLE " + userRoleValue);
        System.out.println("TEST EMAIL " + emailValue);
        System.out.println("TEST PASS " + passwordValue);

    }

    /***
     * Getting data from csv file for ReviewDoctor
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getReviewDoctorData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForReviewDoctor(String userRole, String dashboard, String infoBox, String latestStudies,
                                        String patientsList, String createPatient, String editPatient, String createDevice,
                                        String editDevice, String devicesList, String amplifiersList, String createAmplifier,
                                        String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                        String studiesList, String openStudy, String usersList, String createUser, String editUser) {
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for ReviewDoctor
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void reviewDoctorUser_LoginRoleVerificaiton() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_LoginRoleVerification");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_LoginRoleVerification</p>");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);
    }

    /***
     * Implementing patient list authorization for ReviewDoctor
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void reviewDoctorUser_PatientListAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_PatientListAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_PatientListAction</p>");
        actionAccessScriptForPatientModule_List(patientsListValue);
    }

    /***
     * Implementing patient create action authorization for ReviewDoctor
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void reviewDoctorUser_PatientCreateAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_PatientCreateAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_PatientCreateAction</p>");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);
    }

    /***
     * Implementing patient edit action authorization for ReviewDoctor
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void reviewDoctorUser_PatientEditAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_PatientEditAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_PatientEditAction</p>");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);
    }

    /***
     * Implementing facility list authorization for ReviewDoctor
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 7)
    public void reviewDoctorUser_FacilityListAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_FacilityListAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_FacilityListAction</p>");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);
    }

    /***
     * Implementing facility create action authorization for ReviewDoctor
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 8)
    public void reviewDoctorUser_FacilityCreateAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_FacilityCreateAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_FacilityCreateAction</p>");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "RD");
    }

    /***
     * Implementing facility edit action authorization for ReviewDoctor
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 9)
    public void reviewDoctorUser_FacilityEditAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_FacilityEditAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_FacilityEditAction</p>");
        actionAccessScriptForFacilityModule_Edit(facilitiesListValue, editFacilityValue, "RD");
    }

    /***
     * Implementing amplifier list authorization for ReviewDoctor
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 10)
    public void reviewDoctorUser_AmplifierListAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_AmplifierListAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_AmplifierListAction</p>");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);
    }

    /***
     * Implementing amplifier create action authorization for ReviewDoctor
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 11)
    public void reviewDoctorUser_AmplifierCreateAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_AmplifierCreateAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_AmplifierCreateAction</p>");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "RD");
    }

    /***
     * Implementing amplifier edit action authorization for ReviewDoctor
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 12)
    public void reviewDoctorUser_AmplifierEditAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_AmplifierEditAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_AmplifierEditAction</p>");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "RD");
    }

    /***
     * Implementing device list authorization for ReviewDoctor
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 13)
    public void reviewDoctorUser_DeviceListAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_DeviceListAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_DeviceListAction</p>");
        actionAccessScriptForDeviceModule_List(devicesListValue);
    }

    /***
     * Implementing device create action authorization for ReviewDoctor
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 14)
    public void reviewDoctorUser_DeviceCreateAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_DeviceCreateAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_DeviceCreateAction</p>");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);
    }

    /***
     * Implementing device edit action authorization for ReviewDoctor
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 15)
    public void reviewDoctorUser_DeviceEditAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_DeviceEditAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_DeviceEditAction</p>");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);
    }

    /***
     * Implementing user list authorization for ReviewDoctor
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 16)
    public void reviewDoctorUser_UserListAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_UserListAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_UserListAction</p>");
        actionAccessScriptForUserModule_List(usersListValue);
    }

    /***
     * Implementing user create action authorization for ReviewDoctor
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 17)
    public void reviewDoctorUser_UserCreateAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_UserCreateAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_UserCreateAction</p>");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "RD");
    }

    /***
     * Implementing user edit action authorization for ReviewDoctor
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 18)
    public void reviewDoctorUser_UserEditAction() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_UserEditAction");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_UserEditAction</p>");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "RD");
    }

}
