package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class FacilityAdminActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for FacilityAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getFacilityAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForFacilityAdmin(String userRole, String email, String password) {
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

        System.out.println("TEST ROLE " + userRoleValue);
        System.out.println("TEST EMAIL " + emailValue);
        System.out.println("TEST PASS " + passwordValue);

    }

    /***
     * Getting data from csv file for FacilityAdmin
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getFacilityAdminData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForFacilityAdmin(String userRole, String dashboard, String infoBox, String latestStudies,
                                         String patientsList, String createPatient, String editPatient, String createDevice,
                                         String editDevice, String devicesList, String amplifiersList, String createAmplifier,
                                         String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                         String studiesList, String openStudy, String usersList, String createUser, String editUser) {
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for FacilityAdmin
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void facilityAdminUser_LoginRoleVerificaiton() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_LoginRoleVerification");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_LoginRoleVerification</p>");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);
    }

    /***
     * Implementing patient list authorization for FacilityAdmin
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void facilityAdminUser_PatientListAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_PatientListAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_PatientListAction</p>");
        actionAccessScriptForPatientModule_List(patientsListValue);
    }

    /***
     * Implementing patient create action authorization for FacilityAdmin
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void facilityAdminUser_PatientCreateAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_PatientCreateAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_PatientCreateAction</p>");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);
    }

    /***
     * Implementing patient edit action authorization for FacilityAdmin
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void facilityAdminUser_PatientEditAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_PatientEditAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_PatientEditAction</p>");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);
    }

    /***
     * Implementing facility list authorization for FacilityAdmin
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 7)
    public void facilityAdminUser_FacilityListAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_FacilityListAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_FacilityListAction</p>");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);
    }

    /***
     * Implementing facility create action authorization for FacilityAdmin
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 8)
    public void facilityAdminUser_FacilityCreateAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_FacilityCreateAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_FacilityCreateAction</p>");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "FA");
    }

    /***
     * Implementing facility edit action authorization for FacilityAdmin
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 9)
    public void facilityAdminUser_FacilityEditAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_FacilityEditAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_FacilityEditAction</p>");
        actionAccessScriptForFacilityModule_Edit_FA(facilitiesListValue, editFacilityValue, "FacilityAdmin");
    }

    /***
     * Implementing amplifier list authorization for FacilityAdmin
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 10)
    public void facilityAdminUser_AmplifierListAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_AmplifierListAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_AmplifierListAction</p>");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);
    }

    /***
     * Implementing amplifier create action authorization for FacilityAdmin
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 11)
    public void facilityAdminUser_AmplifierCreateAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_AmplifierCreateAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_AmplifierCreateAction</p>");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "FA");
    }

    /***
     * Implementing amplifier edit action authorization for FacilityAdmin
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 12)
    public void facilityAdminUser_AmplifierEditAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_AmplifierEditAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_AmplifierEditAction</p>");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "FA");
    }

    /***
     * Implementing device list authorization for FacilityAdmin
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 13)
    public void facilityAdminUser_DeviceListAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_DeviceListAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_DeviceListAction</p>");
        actionAccessScriptForDeviceModule_List(devicesListValue);
    }

    /***
     * Implementing device create action authorization for FacilityAdmin
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 14)
    public void facilityAdminUser_DeviceCreateAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_DeviceCreateAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_DeviceCreateAction</p>");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);
    }

    /***
     * Implementing device edit action authorization for FacilityAdmin
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 15)
    public void facilityAdminUser_DeviceEditAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_DeviceEditAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_DeviceEditAction</p>");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);
    }

    /***
     * Implementing user list authorization for FacilityAdmin
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 16)
    public void facilityAdminUser_UserListAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_UserListAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_UserListAction</p>");
        actionAccessScriptForUserModule_List(usersListValue);
    }

    /***
     * Implementing user create action authorization for FacilityAdmin
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 17)
    public void facilityAdminUser_UserCreateAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_UserCreateAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_UserCreateAction</p>");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "FA");
    }

    /***
     * Implementing user edit action authorization for FacilityAdmin
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 18)
    public void facilityAdminUser_UserEditAction() {
        Logger.startTestCase("ValidatePageAccessForFacilityAdmin_UserEditAction");
        Reporter.log("<p>ValidatePageAccessForFacilityAdmin_UserEditAction</p>");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "FA");
    }

}
