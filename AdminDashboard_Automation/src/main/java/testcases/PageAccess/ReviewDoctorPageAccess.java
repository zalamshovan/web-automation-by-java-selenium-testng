package testcases.PageAccess;

import dataprovider.DataProviderClass;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class ReviewDoctorPageAccess extends BasePageAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for ReviewDoctor
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getReviewDoctorCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForReviewDoctor(String userRole, String email, String password) {
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

        System.out.println("TEST ROLE " + userRoleValue);
        System.out.println("TEST EMAIL " + emailValue);
        System.out.println("TEST PASS " + passwordValue);

    }

    /***
     * Getting data from csv file for reviewDoctor
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getReviewDoctorData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForReviewDoctor(String userRole, String dashboard, String infoBox, String latestStudies,
                                       String patientsList, String createPatient, String editPatient,
                                       String createDevice, String editDevice, String devicesList,
                                       String amplifiersList, String createAmplifier,
                                        String editAmplifier, String facilitiesList, String createFacility,
                                       String editFacility, String studiesList, String openStudy, String usersList,
                                       String createUser, String editUser) {
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for reviewDoctor
     * Passing email and password to BasePageAccess method pageAccessScriptForLogin
     * Passing user role and dashboard authorization value to BasePageAcces method pageAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void reviewDoctorUser_LoginRoleVerificaiton() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_LoginRoleVerification");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_LoginRoleVerification</p>");
        pageAccessScriptForLogin(emailValue, passwordValue);
        pageAccessScriptForUserRole(userRoleValue, dashboardValue);
    }

    /***
     * Implementing dashboard element access verification for reviewDoctor
     * Passing dashboard authorization value, infoBox value, latestStudies value to BasePageAccess method pageAccessScriptForDashboard
     */
    @Test(priority = 4)
    public void reviewDoctorUser_DashboardVerificaiton() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_DashboardVerification");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_DashboardVerification</p>");
        pageAccessScriptForDashboard(dashboardValue, infoBoxValue, latestStudiesValue);
    }

    /***
     * Implementing patient module access authorization for reviewDoctor
     * Passing patient list value, create patient value, edit patient value to BasePageAccess method pageAccessScriptForPatientModule
     */
    @Test(priority = 5)
    public void reviewDoctorUser_PatientModule() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_PatientModule");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_PatientModule</p>");
        pageAccessScriptForPatientModule(patientsListValue, createPatientValue, editPatientValue);
    }

    /***
     * Implementing study module access authorization for reviewDoctor
     * Passing study list value, open study value to BasePageAccess method pageAccessScriptForStudyModule
     */
    @Test(priority = 6)
    public void reviewDoctorUser_StudyModule() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_StudyModule");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_StudyModule</p>");
        pageAccessScriptForStudyModule(studiesListValue, openStudyValue);
    }

    /***
     * Implementing facility module access authorization for reviewDoctor
     * Passing facility list value, create facility value, edit facility value to BasePageAccess method pageAccessScriptForFacilityModule
     */
    @Test(priority = 7)
    public void reviewDoctorUser_FacilityModule() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_FacilityModule");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_FacilityModule</p>");
        pageAccessScriptForFacilityModule(facilitiesListValue, createFacilityValue, editFacilityValue);
    }

    /***
     * Implementing amplifier module access authorization for reviewDoctor
     * Passing amplifier list value, create amplifier value, edit amplifier value to BasePageAccess method pageAccessScriptForAmplifierModule
     */
    @Test(priority = 8)
    public void reviewDoctorUser_AmplifierModule() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_AmplifierModule");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_AmplifierModule</p>");
        pageAccessScriptForAmplifierModule(amplifiersListValue, createAmplifierValue, editAmplifierValue);
    }

    /***
     * Implementing device module access authorization for reviewDoctor
     * Passing device list value, create device value, edit device value to BasePageAccess method pageAccessScriptForDeviceModule
     */
    @Test(priority = 9)
    public void reviewDoctorUser_DeviceModule() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_DeviceModule");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_DeviceModule</p>");
        pageAccessScriptForDeviceModule(devicesListValue, createDeviceValue, editDeviceValue);
    }

    /***
     * Implementing user module access authorization for reviewDoctor
     * Passing user list value, create user value, edit user value to BasePageAccess method pageAccessScriptForUserModule
     */
    @Test(priority = 10)
    public void reviewDoctorUser_UserModule() {
        Logger.startTestCase("ValidatePageAccessForReviewDoctor_UserModule");
        Reporter.log("<p>ValidatePageAccessForReviewDoctor_UserModule</p>");
        pageAccessScriptForUserModule(usersListValue, createUserValue, editUserValue);
    }
}
