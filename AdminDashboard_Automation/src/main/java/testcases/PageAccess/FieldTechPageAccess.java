package testcases.PageAccess;

import dataprovider.DataProviderClass;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class FieldTechPageAccess extends BasePageAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for FieldTech
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getFieldTechCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForFieldTech(String userRole, String email, String password) {
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

        System.out.println("TEST ROLE " + userRoleValue);
        System.out.println("TEST EMAIL " + emailValue);
        System.out.println("TEST PASS " + passwordValue);

    }

    /***
     * Getting data from csv file for fieldTech
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getFieldTechData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForFieldTech(String userRole, String dashboard, String infoBox, String latestStudies,
                                    String patientsList, String createPatient, String editPatient, String createDevice,
                                    String editDevice, String devicesList, String amplifiersList,
                                    String createAmplifier, String editAmplifier, String facilitiesList,
                                    String createFacility, String editFacility, String studiesList, String openStudy,
                                    String usersList, String createUser, String editUser) {
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for fieldTech
     * Passing email and password to BasePageAccess method pageAccessScriptForLogin
     * Passing user role and dashboard authorization value to BasePageAcces method pageAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void fieldTechUser_LoginRoleVerificaiton() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_LoginRoleVerification");
        Reporter.log("<p>ValidatePageAccessForFieldTech_LoginRoleVerification</p>");
        pageAccessScriptForLogin(emailValue, passwordValue);
        pageAccessScriptForUserRole(userRoleValue, dashboardValue);
    }

    /***
     * Implementing dashboard element access verification for fieldTech
     * Passing dashboard authorization value, infoBox value, latestStudies value to BasePageAccess method pageAccessScriptForDashboard
     */
    @Test(priority = 4)
    public void fieldTechUser_DashboardVerificaiton() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_DashboardVerification");
        Reporter.log("<p>ValidatePageAccessForFieldTech_DashboardVerification</p>");
        pageAccessScriptForDashboard(dashboardValue, infoBoxValue, latestStudiesValue);
    }

    /***
     * Implementing patient module access authorization for fieldTech
     * Passing patient list value, create patient value, edit patient value to BasePageAccess method pageAccessScriptForPatientModule
     */
    @Test(priority = 5)
    public void fieldTechUser_PatientModule() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_PatientModule");
        Reporter.log("<p>ValidatePageAccessForFieldTech_PatientModule</p>");
        pageAccessScriptForPatientModule(patientsListValue, createPatientValue, editPatientValue);
    }

    /***
     * Implementing study module access authorization for fieldTech
     * Passing study list value, open study value to BasePageAccess method pageAccessScriptForStudyModule
     */
    @Test(priority = 6)
    public void fieldTechUser_StudyModule() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_StudyModule");
        Reporter.log("<p>ValidatePageAccessForFieldTech_StudyModule</p>");
        pageAccessScriptForStudyModule(studiesListValue, openStudyValue);
    }

    /***
     * Implementing facility module access authorization for fieldTech
     * Passing facility list value, create facility value, edit facility value to BasePageAccess method pageAccessScriptForFacilityModule
     */
    @Test(priority = 7)
    public void fieldTechUser_FacilityModule() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_FacilityModule");
        Reporter.log("<p>ValidatePageAccessForFieldTech_FacilityModule</p>");
        pageAccessScriptForFacilityModule(facilitiesListValue, createFacilityValue, editFacilityValue);
    }

    /***
     * Implementing amplifier module access authorization for fieldTech
     * Passing amplifier list value, create amplifier value, edit amplifier value to BasePageAccess method pageAccessScriptForAmplifierModule
     */
    @Test(priority = 8)
    public void fieldTechUser_AmplifierModule() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_AmplifierModule");
        Reporter.log("<p>ValidatePageAccessForFieldTech_AmplifierModule</p>");
        pageAccessScriptForAmplifierModule(amplifiersListValue, createAmplifierValue, editAmplifierValue);
    }

    /***
     * Implementing device module access authorization for fieldTech
     * Passing device list value, create device value, edit device value to BasePageAccess method pageAccessScriptForDeviceModule
     */
    @Test(priority = 9)
    public void fieldTechUser_DeviceModule() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_DeviceModule");
        Reporter.log("<p>ValidatePageAccessForFieldTech_DeviceModule</p>");
        pageAccessScriptForDeviceModule(devicesListValue, createDeviceValue, editDeviceValue);
    }

    /***
     * Implementing user module access authorization for fieldTech
     * Passing user list value, create user value, edit user value to BasePageAccess method pageAccessScriptForUserModule
     */
    @Test(priority = 10)
    public void fieldTechUser_UserModule() {
        Logger.startTestCase("ValidatePageAccessForFieldTech_UserModule");
        Reporter.log("<p>ValidatePageAccessForFieldTech_UserModule</p>");
        pageAccessScriptForUserModule(usersListValue, createUserValue, editUserValue);
    }
}
