package testcases.PageAccess;

import dataprovider.DataProviderClass;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class SupportPageAccess extends BasePageAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for Support
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSupportCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSupport(String userRole, String email, String password) {
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

        System.out.println("TEST ROLE " + userRoleValue);
        System.out.println("TEST EMAIL " + emailValue);
        System.out.println("TEST PASS " + passwordValue);

    }

    /***
     * Getting data from csv file for support
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getSupportData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForSupport(String userRole, String dashboard, String infoBox, String latestStudies,
                                  String patientsList, String createPatient, String editPatient, String createDevice,
                                  String editDevice, String devicesList, String amplifiersList, String createAmplifier,
                                     String editAmplifier, String facilitiesList, String createFacility,
                                  String editFacility, String studiesList, String openStudy, String usersList,
                                  String createUser, String editUser) {
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for support
     * Passing email and password to BasePageAccess method pageAccessScriptForLogin
     * Passing user role and dashboard authorization value to BasePageAcces method pageAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void supportUser_LoginRoleVerificaiton() {
        Logger.startTestCase("ValidatePageAccessForSupport_LoginRoleVerification");
        Reporter.log("<p>ValidatePageAccessForSupport_LoginRoleVerification</p>");
        pageAccessScriptForLogin(emailValue, passwordValue);
        pageAccessScriptForUserRole(userRoleValue, dashboardValue);
    }

    /***
     * Implementing dashboard element access verification for support
     * Passing dashboard authorization value, infoBox value, latestStudies value to BasePageAccess method pageAccessScriptForDashboard
     */
    @Test(priority = 4)
    public void supportUser_DashboardVerificaiton() {
        Logger.startTestCase("ValidatePageAccessForSupport_DashboardVerification");
        Reporter.log("<p>ValidatePageAccessForSupport_DashboardVerification</p>");
        pageAccessScriptForDashboard(dashboardValue, infoBoxValue, latestStudiesValue);
    }

    /***
     * Implementing patient module access authorization for support
     * Passing patient list value, create patient value, edit patient value to BasePageAccess method pageAccessScriptForPatientModule
     */
    @Test(priority = 5)
    public void supportUser_PatientModule() {
        Logger.startTestCase("ValidatePageAccessForSupport_PatientModule");
        Reporter.log("<p>ValidatePageAccessForSupport_PatientModule</p>");
        pageAccessScriptForPatientModule(patientsListValue, createPatientValue, editPatientValue);
    }

    /***
     * Implementing study module access authorization for support
     * Passing study list value, open study value to BasePageAccess method pageAccessScriptForStudyModule
     */
    @Test(priority = 6)
    public void supportUser_StudyModule() {
        Logger.startTestCase("ValidatePageAccessForSupport_StudyModule");
        Reporter.log("<p>ValidatePageAccessForSupport_StudyModule</p>");
        pageAccessScriptForStudyModule(studiesListValue, openStudyValue);
    }

    /***
     * Implementing facility module access authorization for support
     * Passing facility list value, create facility value, edit facility value to BasePageAccess method pageAccessScriptForFacilityModule
     */
    @Test(priority = 7)
    public void supportUser_FacilityModule() {
        Logger.startTestCase("ValidatePageAccessForSupport_FacilityModule");
        Reporter.log("<p>ValidatePageAccessForSupport_FacilityModule</p>");
        pageAccessScriptForFacilityModule(facilitiesListValue, createFacilityValue, editFacilityValue);
    }

    /***
     * Implementing amplifier module access authorization for support
     * Passing amplifier list value, create amplifier value, edit amplifier value to BasePageAccess method pageAccessScriptForAmplifierModule
     */
    @Test(priority = 8)
    public void supportUser_AmplifierModule() {
        Logger.startTestCase("ValidatePageAccessForSupport_AmplifierModule");
        Reporter.log("<p>ValidatePageAccessForSupport_AmplifierModule</p>");
        pageAccessScriptForAmplifierModule(amplifiersListValue, createAmplifierValue, editAmplifierValue);
    }

    /***
     * Implementing device module access authorization for support
     * Passing device list value, create device value, edit device value to BasePageAccess method pageAccessScriptForDeviceModule
     */
    @Test(priority = 9)
    public void supportUser_DeviceModule() {
        Logger.startTestCase("ValidatePageAccessForSupport_DeviceModule");
        Reporter.log("<p>ValidatePageAccessForSupport_DeviceModule</p>");
        pageAccessScriptForDeviceModule(devicesListValue, createDeviceValue, editDeviceValue);
    }

    /***
     * Implementing user module access authorization for support
     * Passing user list value, create user value, edit user value to BasePageAccess method pageAccessScriptForUserModule
     */
    @Test(priority = 10)
    public void supportUser_UserModule() {
        Logger.startTestCase("ValidatePageAccessForSupport_UserModule");
        Reporter.log("<p>ValidatePageAccessForSupport_UserModule</p>");
        pageAccessScriptForUserModule(usersListValue, createUserValue, editUserValue);
    }
}
