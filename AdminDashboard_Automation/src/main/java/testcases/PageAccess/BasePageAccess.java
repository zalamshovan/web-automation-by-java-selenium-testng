package testcases.PageAccess;

import helper.Logger;
import org.testng.Reporter;
import pages.PagesFactory;
import pages.ui.PageClasses.*;
import pages.ui.PageClasses.CreateEditPages.*;
import pages.ui.PageClasses.ListPages.*;
import testcases.BaseTest;

public class BasePageAccess extends BaseTest {

    LoginPage loginPage;
    DashboardPage dashboardPage;
    ProfilePage profilePage;
    MenuSidebarPage menuSidebarPage;
    PatientsPage patientsPage;
    HeaderPanel headerPanel;
    StudiesPage studiesPage;
    PatientCreatePage patientCreatePage;
    PatientEditPage patientEditPage;
    RemoteViewerPage remoteViewerPage;
    FacilityPage facilityPage;
    FacilityCreatePage facilityCreatePage;
    FacilityEditPage facilityEditPage;
    AmplifierPage amplifierPage;
    AmplifierCreatePage amplifierCreatepage;
    AmplifierEditPage amplifierEditPage;
    DevicePage devicePage;
    DeviceCreatePage deviceCreatePage;
    DeviceEditPage deviceEditPage;
    UserPage userPage;
    UserCreatePage userCreatePage;
    UserEditPage userEditPage;

    /***
     * Implementing test case to validate Login page
     * @param email
     * @param password
     */
    public void pageAccessScriptForLogin(String email, String password)
    {
        loginPage = PagesFactory.getLoginPage();

        loginPage.verifyLoginPage();
        Logger.info("Rendr Neuro Portal Page Loaded");
        Reporter.log("<p>Rendr Neuro Portal Page Loaded</p>");
        loginPage.typeUserName(email);
        loginPage.typePassword(password);
        Logger.info("User Credentials Given");
        Reporter.log("<p>User Credentials Given</p>");
        loginPage.clickLoginButton();
        Logger.info("Login button clicked");
        Reporter.log("<p>Login button clicked</p>");
        loginPage.loginButtonAppearance();
    }

    /***
     * Implementing test case to validate user role
     * @param userRole
     * @param dashboard
     */
    public void pageAccessScriptForUserRole(String userRole, String dashboard)
    {
        dashboardPage = PagesFactory.getDashboardPage();
        headerPanel = PagesFactory.getheaderPanel();
        profilePage = PagesFactory.getProfilePage();

        headerPanel.yourProfileButtonClick();
        Logger.info("Your Profile button clicked");
        Reporter.log("<p>Your Profile button clicked</p>");
        profilePage.verifyProfilePageNavigation();
        Logger.info("Successfully navigated to the profile page");
        Reporter.log("<p>Successfully navigated to the profile page</p>");
        profilePage.verifyUserRole(userRole);
        Logger.info("Support role matched");
        Reporter.log("<p>Support role matched</p>");
    }

    /***
     * Implementing test case to validate patient module access
     * @param patientsList
     * @param createPatient
     * @param editPatient
     */
    public void pageAccessScriptForPatientModule(String patientsList, String createPatient, String editPatient)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();
        patientEditPage = PagesFactory.getPatientEditPage();

        menuSidebarPage.patientPageVerification(patientsList);
        Logger.info("Patient page pane verified");
        Reporter.log("<p>Patient page pane verified</p>");
        patientsPage.verifyPatientPageNavigation(patientsList);
        Logger.info("Patient page navigation verified");
        Reporter.log("<p>Patient page navigation verified</p>");
        patientsPage.verifyCreateButtonClick(createPatient);
        Logger.info("Create Patient button click verified");
        Reporter.log("<p>Create Patient button click verified</p>");
        patientCreatePage.verifyCreatePatientPageNavigation(createPatient);
        Logger.info("Create patient page verified");
        Reporter.log("<p>Create patient page verified</p>");
        menuSidebarPage.patientPageVerification(patientsList);
        Logger.info("Patient page pane verified");
        Reporter.log("<p>Patient page pane verified</p>");
        patientsPage.verifyPatientPageNavigation(patientsList);
        Logger.info("Patient page navigation verified");
        Reporter.log("<p>Patient page navigation verified</p>");
        patientsPage.verifyEditButtonClick(editPatient);
        Logger.info("Edit button click verified");
        Reporter.log("<p>Edit button click verified</p>");
        patientEditPage.verifyEditPatientPageNavigation(editPatient);
        Logger.info("Edit patient page navigation verified");
        Reporter.log("<p>Edit patient page navigation verified</p>");
    }

    /***
     * Implementing test case to validate study module access
     * @param studiesList
     * @param openStudy
     */
    public void pageAccessScriptForStudyModule(String studiesList, String openStudy)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        studiesPage = PagesFactory.getStudiesPage();
        remoteViewerPage = PagesFactory.getRemoteViewerPage();

        menuSidebarPage.studyPageVerification(studiesList);
        Logger.info("Study page pane verified");
        Reporter.log("<p>Study page pane verified</p>");
        studiesPage.verifyStudiesPageNavigation(studiesList);
        Logger.info("Studies page verified");
        Reporter.log("<p>Studies page verified</p>");
        studiesPage.verifyOpenStudyButtonClick(openStudy);
        Logger.info("Open study button verified");
        Reporter.log("<p>Open study button verified</p>");
        remoteViewerPage.verifyRemoteViewerNavigation(openStudy);
        Logger.info("Remote viewer navigation verified");
        Reporter.log("<p>Remote viewer navigation verified</p>");
        remoteViewerPage.goBackToStudiesPage(openStudy);
        Logger.info("Successfully navigated back to the studies page");
        Reporter.log("<p>Successfully navigated back to the studies page</p>");
        studiesPage.verifyStudiesPageNavigation(studiesList);
        Logger.info("Studies page verified");
        Reporter.log("<p>Studies page verified</p>");
    }

    /***
     * Implementing test case to validate facility module access
     * @param facilitiesList
     * @param createFacility
     * @param editFacility
     */
    public void pageAccessScriptForFacilityModule(String facilitiesList, String createFacility, String editFacility)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityCreatePage = PagesFactory.getFacilityCreatePage();
        facilityEditPage = PagesFactory.getFacilityEditPage();

        menuSidebarPage.facilityPageVerification(facilitiesList);
        Logger.info("Facility page pane verified");
        Reporter.log("<p>Facility page pane verified</p>");
        facilityPage.verifyFacilityPageNavigation(facilitiesList);
        Logger.info("Facilities page navigation verified");
        Reporter.log("<p>Facilities page navigation verified</p>");
        facilityPage.verifyCreateButtonClick(createFacility);
        Logger.info("Create facility button verified");
        Reporter.log("<p>Create facility button verified</p>");
        facilityCreatePage.verifyCreateFacilityPageNavigation(createFacility);
        Logger.info("Create Facilities page navigation verified");
        Reporter.log("<p>Create Facilities page navigation verified</p>");
        menuSidebarPage.facilityPageVerification(facilitiesList);
        Logger.info("Facility page pane verified");
        Reporter.log("<p>Facility page pane verified</p>");
        facilityPage.verifyFacilityPageNavigation(facilitiesList);
        Logger.info("Facility page navigation verified");
        Reporter.log("<p>Facility page navigation verified</p>");
        facilityPage.verifyEditButtonClick(editFacility);
        Logger.info("Edit button click verified");
        Reporter.log("<p>Edit button click verified</p>");
        facilityEditPage.verifyEditFacilityPageNavigation(editFacility);
        Logger.info("Edit facility page navigation verified");
        Reporter.log("<p>Edit facility page navigation verified</p>");
    }

    /***
     * Implementing test case to validate amplifier module access
     * @param amplifiersList
     * @param createAmplifier
     * @param editAmplifier
     */
    public void pageAccessScriptForAmplifierModule(String amplifiersList, String createAmplifier, String editAmplifier)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        amplifierPage = PagesFactory.getAmplifierPage();
        amplifierCreatepage = PagesFactory.getAmplifierCreatePage();
        amplifierEditPage = PagesFactory.getAmplifierEditPage();

        menuSidebarPage.amplifierPageVerification(amplifiersList);
        Logger.info("Amplifeir page pane verified");
        Reporter.log("<p>Amplifeir page pane verified</p>");
        amplifierPage.verifyAmplifierPageNavigation(amplifiersList);
        Logger.info("Amplifiers page navigation verified");
        Reporter.log("<p>Amplifiers page navigation verified</p>");
        amplifierPage.verifyCreateButtonClick(createAmplifier);
        Logger.info("Create amplifier button verified");
        Reporter.log("<p>Create amplifier button verified</p>");
        amplifierCreatepage.verifyCreateAmplifierPageNavigation(createAmplifier);
        Logger.info("Create Amplifier page navigation verified");
        Reporter.log("<p>Create Amplifier page navigation verified</p>");
        menuSidebarPage.amplifierPageVerification(amplifiersList);
        Logger.info("Amplifeir page pane verified");
        Reporter.log("<p>Amplifeir page pane verified</p>");
        amplifierPage.verifyAmplifierPageNavigation(amplifiersList);
        Logger.info("Amplifier page navigation verified");
        Reporter.log("<p>Amplifier page navigation verified</p>");
        amplifierPage.verifyEditButtonClick(editAmplifier);
        Logger.info("Edit button verified");
        Reporter.log("<p>Edit button verified</p>");
        amplifierEditPage.verifyEditAmplifierPageNavigation(editAmplifier);
        Logger.info("Edit amplifier page navigation verified");
        Reporter.log("<p>Edit amplifier page navigation verified</p>");
    }

    /***
     * Implementing test case to validate device module access
     * @param devicesList
     * @param createDevice
     * @param editDevice
     */
    public void pageAccessScriptForDeviceModule(String devicesList, String createDevice, String editDevice)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        devicePage = PagesFactory.getDevicePage();
        deviceCreatePage = PagesFactory.getCreateDevicePage();
        deviceEditPage = PagesFactory.getEditDevicePage();

        menuSidebarPage.devicePageVerification(devicesList);
        Logger.info("Device page pane verified");
        Reporter.log("<p>Device page pane verified</p>");
        devicePage.verifyDevicesPageNavigation(devicesList);
        Logger.info("Devices page navigation verified");
        Reporter.log("<p>Devices page navigation verified</p>");
        devicePage.verifyCreateButtonClick(createDevice);
        Logger.info("Create device button verified");
        Reporter.log("<p>Create device button verified</p>");
        deviceCreatePage.verifyCreateDevicePageNavigation(createDevice);
        Logger.info("Create Device page navigation verified");
        Reporter.log("<p>Create Device page navigation verified</p>");
        menuSidebarPage.devicePageVerification(devicesList);
        Logger.info("Device page pane verified");
        Reporter.log("<p>Device page pane verified</p>");
        devicePage.verifyDevicesPageNavigation(devicesList);
        Logger.info("Device page navigation verified");
        Reporter.log("<p>Device page navigation verified</p>");
        devicePage.verifyEditButtonClick(editDevice);
        Logger.info("Edit button verified");
        Reporter.log("<p>Edit button verified</p>");
        deviceEditPage.verifyEditDevicePageNavigation(editDevice);
        Logger.info("Edit Device page navigation verified");
        Reporter.log("<p>Edit Device page navigation verified</p>");
    }

    /***
     * Implementing test case to validate user module access
     * @param usersList
     * @param createUser
     * @param editUser
     */
    public void pageAccessScriptForUserModule(String usersList, String createUser, String editUser)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();
        userEditPage = PagesFactory.getUserEditPage();

        menuSidebarPage.userPageVerification(usersList);
        Logger.info("User page pane verified");
        Reporter.log("<p>User page pane verified</p>");
        userPage.verifyUserPageNavigation(usersList);
        Logger.info("User page navigation verified");
        Reporter.log("<p>User page navigation verified</p>");
        userPage.verifyCreateButtonClick(createUser);
        Logger.info("Create user button verified");
        Reporter.log("<p>Create user button verified</p>");
        userCreatePage.verifyCreateUserPageNavigation(createUser);
        Logger.info("Create User page navigation verified");
        Reporter.log("<p>Create User page navigation verified</p>");
        menuSidebarPage.userPageVerification(usersList);
        Logger.info("User page pane verified");
        Reporter.log("<p>User page pane verified</p>");
        userPage.verifyUserPageNavigation(usersList);
        Logger.info("User page navigation verified");
        Reporter.log("<p>User page navigation verified</p>");
        userPage.verifyEditButtonClick(editUser);
        Logger.info("Edit button verified");
        Reporter.log("<p>Edit button verified</p>");
        userEditPage.verifyEditUserPageNavigation(editUser);
        Logger.info("Edit user page navigation verified");
        Reporter.log("<p>Edit user page navigation verified</p>");
    }

    /***
     * Implementing test case to validate dashboard element access
     * @param dashboard
     * @param infoBox
     * @param latestStudies
     */
    public void pageAccessScriptForDashboard(String dashboard, String infoBox, String latestStudies)
    {
        dashboardPage = PagesFactory.getDashboardPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        headerPanel = PagesFactory.getheaderPanel();

        menuSidebarPage.dashboardPageVerification(dashboard);
        Logger.info("Dashboard page pane verified");
        Reporter.log("<p>Dashboard page pane verified</p>");
        headerPanel.selectFacilityFromDropdown("MOBILEMEDTEK");
        Logger.info("Facility selection verified");
        Reporter.log("<p>Facility selection verified</p>");
        dashboardPage.verifyDashboardPageNavigation();
        Logger.info("Dashboard page navigation verified");
        Reporter.log("<p>Dashboard page navigation verified</p>");
        dashboardPage.verifyInfoBox(infoBox);
        Logger.info("Info box verified");
        Reporter.log("<p>Info box verified</p>");
        dashboardPage.verifyLatestStudiesCard(latestStudies);
        Logger.info("Latest studies verified");
        Reporter.log("<p>Latest studies verified</p>");
    }

}
