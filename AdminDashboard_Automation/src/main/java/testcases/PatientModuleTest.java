package testcases;

import dataprovider.DataProviderClass;
import org.testng.Reporter;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.*;
import pages.ui.PageClasses.CreateEditPages.PatientCreatePage;
import pages.ui.PageClasses.CreateEditPages.PatientEditPage;
import pages.ui.PageClasses.ListPages.PatientsPage;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PatientModuleTest extends BaseTest {

    LoginPage loginPage;
    DashboardPage dashboardPage;
    MenuSidebarPage menuSidebarPage;
    PatientsPage patientsPage;
    PatientCreatePage patientCreatePage;
    PatientEditPage patientEditPage;

    Format f = new SimpleDateFormat("MM-dd-yyyy_hh:mm:ss");
    String dateTime = f.format(new Date());
    String userRoleValue;
    String emailValue;
    String passwordValue;


    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

        System.out.println("TEST ROLE " + userRoleValue);
        System.out.println("TEST EMAIL " + emailValue);
        System.out.println("TEST PASS " + passwordValue);

    }

    /***
     * Implementing test case to verify patient list columns
     */
    @Test(priority = 2)
    public void PatientListColumnVerification(){
        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();
        patientsPage = PagesFactory.getPatientsPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        Reporter.log("Patient_List_Column_Verification Test Case Started");
        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyLoginPage();
        Reporter.log("<p>Rendr Neuro Portal Page Loaded </p>");
        loginPage.typeUserName(emailValue);
        loginPage.typePassword(passwordValue);
        Reporter.log("<p>User Credentials Given</p>");
        loginPage.clickLoginButton();
        Reporter.log("<p>Login button clicked</p>");
        loginPage.loginButtonAppearance();
        Reporter.log("<p>Login successful</p>");
        dashboardPage.isDashboardMenuVisible("Yes");
        Reporter.log("<p>Menu bar is visible</p>");
        menuSidebarPage.goToPatientsPage();
        Reporter.log("<p>Patient pane clicked</p>");
        patientsPage.verifyPatientPageNavigation();
        Reporter.log("<p>Successfully navigated to the patients page</p>");
        patientsPage.verifyFirstColumnName("Name");
        Reporter.log("<p>Patient page's first column verified</p>");
        patientsPage.verifySecondColumnName("SSN");
        Reporter.log("<p>Patient page's second column verified</p>");
        patientsPage.verifyThirdColumnName("Patient ID");
        Reporter.log("<p>Patient page's third column verified</p>");
        patientsPage.verifyFourthColumnName("DOB");
        Reporter.log("<p>Patient page's fourth column verified</p>");
        patientsPage.verifyFifthColumnName("Deleted");
        Reporter.log("<p>Patient page's fifth column verified</p>");
    }

    /***
     * Implementing test case to verify create patient functionality
     */
    @Test(priority = 3)
    public void PatientCreateVerification(){
        patientsPage = PagesFactory.getPatientsPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();

        Reporter.log("Patient_Create_Verification Test Case Started");
        menuSidebarPage.goToPatientsPage();
        Reporter.log("<p>Patient pane clicked</p>");
        patientsPage.goToCreatePatientPage();
        Reporter.log("<p>Create Patient button clicked</p>");
        patientCreatePage.verifyCreatePatientPageNavigation();
        Reporter.log("<p>Successfully navigated to the create patient page</p>");
        patientCreatePage.typeFirstName(dateTime+"_First_Name");
        Reporter.log("<p>First name typed</p>");
        patientCreatePage.typeMiddleName(dateTime+"_Middle_Name");
        Reporter.log("<p>Middle name typed</p>");
        patientCreatePage.typeLastName(dateTime+"_Last_Name");
        Reporter.log("<p>Last name typed</p>");
        patientCreatePage.selectSexValueFromDropdown("Male");
        Reporter.log("<p>Male selected from the dropdown</p>");
        patientCreatePage.typeSSN("159753842");
        Reporter.log("<p>SSN typed</p>");
        patientCreatePage.typePatientID(dateTime+"_PatientID");
        Reporter.log("<p>Patient ID typed</p>");
        patientCreatePage.typeDOB();
        Reporter.log("<p>DOB typed</p>");
        patientCreatePage.clickSaveButton();
        Reporter.log("<p>Save button clicked</p>");
        patientCreatePage.MatchToastMessageValue("Patient created!");
        Reporter.log("<p>Toast message matched. Patient got created successfully.</p>");
    }

    /***
     * Implementing test case to search a patient
     */
    @Test(priority = 4)
    public void PatientSearchFunctionality(){
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();

        Reporter.log("Patient_Search_Functionality Test Case Started");
        menuSidebarPage.goToPatientsPage();
        Reporter.log("<p>Patient page pane clicked</p>");
        patientsPage.verifyPatientPageNavigation();
        Reporter.log("<p>Patient page navigation successful</p>");
        patientsPage.searchForPatient(dateTime+"_First_Name");
        Reporter.log("<p>Patient searched by First name</p>");
        patientsPage.verifySearchedPatient(dateTime+"_First_Name");
        Reporter.log("<p>Patient search successful</p>");
        patientsPage.clickOnSearchClearButton();
        Reporter.log("<p>Clear button clicked</p>");
        patientsPage.searchForPatient(dateTime+"_Middle_Name");
        Reporter.log("<p>Patient searched by Middle name</p>");
        patientsPage.verifySearchedPatient(dateTime+"_Middle_Name");
        Reporter.log("<p>Patient search successful</p>");
        patientsPage.clickOnSearchClearButton();
        Reporter.log("<p>Clear button clicked</p>");
        patientsPage.searchForPatient(dateTime+"_Last_Name");
        Reporter.log("<p>Patient searched by Last name</p>");
        patientsPage.verifySearchedPatient(dateTime+"_Last_Name");
        Reporter.log("<p>Patient search successful</p>");
        patientsPage.clickOnSearchClearButton();
        Reporter.log("<p>Clear button clicked</p>");
        patientsPage.searchForPatient("3842");
        Reporter.log("<p>Patient searched by SSN</p>");
        patientsPage.verifySearchedPatient("3842");
        Reporter.log("<p>Patient search successful</p>");
        patientsPage.clickOnSearchClearButton();
        Reporter.log("<p>Clear button clicked</p>");
        patientsPage.searchForPatient();
        Reporter.log("<p>Patient searched by DOB</p>");
        patientsPage.verifySearchedPatient();
        Reporter.log("<p>Patient search successful</p>");
        patientsPage.clickOnSearchClearButton();
        Reporter.log("<p>Clear button clicked</p>");
    }

    /***
     * Implementing test case to verify edit patient functionality
     */
    @Test(priority = 5)
    public void PatientEditFunctionality(){
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientEditPage = PagesFactory.getPatientEditPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();

        Reporter.log("Patient_Edit_Functionality Test Case Started");
        menuSidebarPage.goToPatientsPage();
        Reporter.log("<p>Patient page pane clicked</p>");
        patientsPage.verifyPatientPageNavigation();
        Reporter.log("<p>Patient page navigation successful</p>");
        patientsPage.searchForPatient(dateTime+"_First");
        Reporter.log("<p>Patient searched by First name</p>");
        patientsPage.verifySearchedPatient(dateTime+"_First_Name");
        Reporter.log("<p>Patient search successful</p>");
        patientsPage.clickOnEditButton(dateTime+"_First_Name");
        Reporter.log("<p>Patient edit button clicked</p>");
        patientEditPage.verifyEditPatientPageNavigation();
        Reporter.log("<p>Successfully navigated to the edit patient page</p>");
        patientEditPage.typeFirstName(dateTime + "_Edited");
        Reporter.log("<p>First name typed</p>");
        patientEditPage.typeMiddleName(dateTime + "_Edited");
        Reporter.log("<p>Middle name typed</p>");
        patientEditPage.typeLastName(dateTime + "_Edited");
        Reporter.log("<p>Last name typed</p>");
        patientEditPage.selectSexValueFromDropdown("Female");
        Reporter.log("<p>Male selected from the dropdown</p>");
        patientEditPage.typeSSN("213738840");
        Reporter.log("<p>SSN typed</p>");
        patientEditPage.typePatientID("PatientID_Edited");
        Reporter.log("<p>Patient ID typed</p>");
        patientEditPage.typeDOB();
        Reporter.log("<p>DOB typed</p>");
        patientEditPage.clickSaveButton();
        Reporter.log("<p>Save button clicked</p>");
        patientEditPage.MatchToastMessageValue("Patient saved!");
        Reporter.log("<p>Toast message matched. Patient got edited successfully.</p>");
        menuSidebarPage.goToPatientsPage();
        Reporter.log("<p>Patient pane clicked</p>");
        patientsPage.verifyPatientPageNavigation();
        Reporter.log("<p>Successfully navigated to the patients page</p>");
        patientsPage.searchForPatient(dateTime + "_Edited");
        Reporter.log("<p>Patient searched by First name</p>");
        patientsPage.verifySearchedPatient(dateTime + "_Edited");
        Reporter.log("<p>Patient search successful</p>");

    }

    /***
     * Implementing test case to verify patient list sort (Name ascending order)
     */
    @Test(priority = 6)
    public void PatientListSortVerificationNameAsc() {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();

        Reporter.log("Patient_List_Sort_Verification_Name_Ascending Test Case Started");
        menuSidebarPage.goToDevicesPage();
        menuSidebarPage.goToPatientsPage();
        Reporter.log("<p>Patient page pane clicked</p>");
        patientsPage.verifyPatientPageNavigation();
        Reporter.log("<p>Patient page navigation successful</p>");
        patientsPage.verifyInitialSortIndicationByName();
        Reporter.log("<p>Patient list initial sort indication matched</p>");
        patientsPage.verifySortedData("Name", "Ascending");
        Reporter.log("<p>Patient list is properly sorted by name in ascending order</p>");

    }

    /***
     * Implementing test case to verify patient list sort (Name descending order)
     */
    @Test(priority = 7)
    public void PatientListSortVerificationNameDesc() {
        patientsPage = PagesFactory.getPatientsPage();

        Reporter.log("Patient_List_Sort_Verification_Name_Descending Test Case Started");
        patientsPage.changeNameSort();
        Reporter.log("<p>Patient list sort is changed to descending order by name</p>");
        patientsPage.verifySortedData("Name","Descending");
        Reporter.log("<p>Patient list is properly sorted by name in descending order</p>");

    }

    /***
     * Implementing test case to verify patient list sort (Patient ID ascending order)
     */
    @Test(priority = 8)
    public void PatientListSortVerificationPatientIDAsc() {
        patientsPage = PagesFactory.getPatientsPage();

        Reporter.log("Patient_List_Sort_Verification_PatientID_Ascending Test Case Started");
        patientsPage.changePatientIDSort();
        Reporter.log("<p>Patient list sort is changed to ascending order by Patient ID</p>");
        patientsPage.verifySortedData("Patient ID","Ascending");
        Reporter.log("<p>Patient list is properly sorted by Patient ID in ascending order</p>");

    }

    /***
     * Implementing test case to verify patient list sort (Patient ID descending order)
     */
    @Test(priority = 9)
    public void PatientListSortVerificationPatientIDDesc() {
        patientsPage = PagesFactory.getPatientsPage();

        Reporter.log("Patient_List_Sort_Verification_PatientID_Descending Test Case Started");
        patientsPage.changePatientIDSort();
        Reporter.log("<p>Patient list sort is changed to descending order by Patient ID</p>");
        patientsPage.verifySortedData("Patient ID","Descending");
        Reporter.log("<p>Patient list is properly sorted by Patient ID in descending order</p>");

    }

    /***
     * Implementing test case to verify patient list sort (DOB ascending order)
     */
    @Test(priority = 10)
    public void PatientListSortVerificationDOBAsc() {
        patientsPage = PagesFactory.getPatientsPage();

        Reporter.log("Patient_List_Sort_Verification_DOB_Ascending Test Case Started");
        patientsPage.changeDOBSort();
        Reporter.log("<p>Patient list sort is changed to ascending order by DOB</p>");
        patientsPage.verifySortedData("DOB","Ascending");
        Reporter.log("<p>Patient list is properly sorted by DOB in ascending order</p>");

    }

    /***
     * Implementing test case to verify patient list sort (DOB descending order)
     */
    @Test(priority = 10)
    public void PatientListSortVerificationDOBDesc() {
        patientsPage = PagesFactory.getPatientsPage();

        Reporter.log("Patient_List_Sort_Verification_DOB_Descending Test Case Started");
        patientsPage.changeDOBSort();
        Reporter.log("<p>Patient list sort is changed to descending order by DOB</p>");
        patientsPage.verifySortedData("DOB","Descending");
        Reporter.log("<p>Patient list is properly sorted by DOB in descending order</p>");

    }

}
