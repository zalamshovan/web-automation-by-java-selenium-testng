package testcases;

import helper.Logger;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.HeaderPanel;
import pages.ui.PageClasses.LoginPage;


public class DashboardPageTest extends BaseTest {

    @SuppressWarnings("unused")

    DashboardPage dashboardPage;
    LoginPage loginPage;
    HeaderPanel headerPanel;

    /***
     * Implementing test case to validate dashboard items visibility
     */
    @Test
    public void TC03_validateDashboardItems(){
        Logger.startTestCase("TC03_validateDashboardItems");
        Reporter.log("ValidateDashboardItems");
        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();
        headerPanel = PagesFactory.getheaderPanel();
        Logger.info("Rendr Neuro Portal Page Loaded");
        Reporter.log("Rendr Neuro Portal Page Loaded");
        loginPage.typeUserName("rendrsuper@yahoo.com");
        loginPage.typePassword("Enosis123");
        Logger.info("User Credentials Given");
        Reporter.log("User Credentials Given");
        loginPage.clickLoginButton();
        Logger.info("Login button clicked");
        Reporter.log("Login button clicked");
        dashboardPage.isDashboardMenuVisible("Yes");
        Assert.assertEquals(headerPanel.getUserLoggedInTitle(),"rendrsuper@yahoo.com", "Logged in user's email was not shown");
        Logger.info("User's email ID is displayed and matched");
        Reporter.log("User's email ID is displayed and matched");
        Assert.assertTrue(dashboardPage.isStudiesInProgressCardPresent(), "Studies in Progress card is not present");
        Assert.assertTrue(dashboardPage.isStudiesForReviewCardPresent(), "Studies for Review card is not present");
        Assert.assertTrue(dashboardPage.isTotalStudiesCardPresent(), "Total Studies card is not present");
        headerPanel.signOut();
        loginPage.loginButtonAppearance();
        Logger.endTestCase("TC03_validateDashboardItems");

    }

}
