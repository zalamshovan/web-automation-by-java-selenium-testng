package testcases;

import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.LoginPage;

public class LoginPageTest extends BaseTest{
    @SuppressWarnings("unused")

    LoginPage loginPage;
    DashboardPage dashboardPage;

    /***
     * Implementing test case to validate login functionality
     */
    @Test
    public void TC01_validateLoginFunctionality(){

        Logger.startTestCase("Login_functionality");
        Reporter.log("Login_functionality");
        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyLoginPage();
        Logger.info("Rendr Neuro Portal Page Loaded");
        Reporter.log("Rendr Neuro Portal Page Loaded");
        loginPage.typeUserName("rendrsuper@yahoo.com");
        loginPage.typePassword("Enosis123");
        Logger.info("User Credentials Given");
        Reporter.log("User Credentials Given");
        loginPage.clickLoginButton();
        Logger.info("Login button clicked");
        Reporter.log("Login button clicked");
        loginPage.loginButtonAppearance();
        Logger.endTestCase("Login_functionality");
    }

    /***
     * Implementing test case to validate invalid login credentials
     */
    @Test
    public void TC02_invalidLogin(){

        Logger.startTestCase("InvalidLoginFunctionality");
        Reporter.log("InvalidLoginFunctionality");
        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyLoginPage();
        Logger.info("Rendr Neuro Portal Page Loaded");
        Reporter.log("Rendr Neuro Portal Page Loaded");
        loginPage.typeUserName("rendrsuper123@yahoo.com");
        loginPage.typePassword("Enosis123000");
        Logger.info("Invalid User Credentials Given");
        Reporter.log("Invalid User Credentials Given");
        loginPage.clickLoginButton();
        Logger.info("Login Button Clicked");
        Reporter.log("Login Button Clicked");
        loginPage.checkInvalidCredentialsMessage();
        Logger.info("Incorrect User name or Password error message is shown");
        Reporter.log("Incorrect User name or Password error message is shown");
        Logger.endTestCase("InvalidLoginFunctionality");
    }

}
