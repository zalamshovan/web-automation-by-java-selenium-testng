package testcases;

import browserutility.Browser;

import helper.Logger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.*;
import sun.rmi.runtime.Log;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;


public class BaseTest {
    @SuppressWarnings("unused")

    /***
     * Implementing before suite logging
     */
    @BeforeSuite(alwaysRun = true)
    public void startExecution() {
        Logger.info("TEST EXECUTION STARTED");
        Reporter.log("TEST EXECUTION STARTED");

    }

    /***
     * Implementing test setup functionality
     * Browser will be launched and will navigate to the portal URL
     */
    @BeforeClass(alwaysRun = true)
    public void setUp() {
        Browser.launchBrowser("chrome");
        Logger.info("Chrome Browser Launched");
        Reporter.log("Chrome Browser Launched");
        Browser.goToUrl("http://staging.rendrneuro.com");
        Logger.info("Navigated to portal");
        Reporter.log("Navigated to portal");

    }

    /***
     * Implementing test tear down functionality
     * Browser focus will be closed and will quit the browser
     */
    @AfterClass(alwaysRun = true)
    public void tearDown() {
        Browser.closeFocusedScreen();
        Browser.quitBrowser();
        Logger.info("Browser Closed");
        Reporter.log("Browser Closed");

    }

    /***
     * Implementing after suite logging
     */
    @AfterSuite(alwaysRun = true)
    public void stopExecution() {
        Reporter.log("TEST EXECUTION FINISHED");
        Reporter.log("############################################");

    }

    /***
     * Implementing the screenshot mechanism on test failure
     */
    @AfterMethod(alwaysRun = true)
    public void ScreenShot(ITestResult results) throws IOException{
        if(ITestResult.FAILURE == results.getStatus()){
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_hhmmss");
            Date date = new Date();
            String strDate= formatter.format(date);
            File srcfileObj = ((TakesScreenshot) Browser.getWebDriver()).getScreenshotAs(OutputType.FILE);
            String DestFilePath = System.getProperty("user.dir") + "\\Screenshots\\" + strDate + ".png";
            File DestFileObj = new File(DestFilePath);
            FileUtils.copyFile(srcfileObj, DestFileObj);
            Reporter.setCurrentTestResult(results);
            Reporter.log("<br><img src='" + DestFileObj + "'height = '300' width = '600'/><br>");
        }
        else if (ITestResult.SUCCESS == results.getStatus()){
            Reporter.log("TC is passed");
        }
    }

}
