package helper;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {
    // Relative path to the config.properties file
    private String pathToConfig = "src\\main\\resources\\";

    public String appURL;
    public String admiUser;
    public String password;


    public ConfigReader() {
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(pathToConfig+"config.properties");
            prop.load(input);
            appURL = prop.getProperty("appurl");
            admiUser = prop.getProperty("admiuser");
            password = prop.getProperty("password");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
