/**
 * Browser class implementation.
 * Author: Jony, Date: September 12, 2018
 */

package browserutility;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.File;
import java.io.IOException;

public final class Browser {

    private static WebDriver webDriver;

    /***
     * Implementing luanch browser functionality
     * @param browserName
     */
    public static void launchBrowser(String browserName) {
        String driverPath = "src\\main\\resources\\webdrivers\\";
        try {
            if (browserName.equalsIgnoreCase("ie")) {
                webDriver = new InternetExplorerDriver();

            } else if (browserName.equalsIgnoreCase("Chrome")) {
                System.setProperty("webdriver.chrome.driver", driverPath +"chromedriver.exe");
                webDriver = new ChromeDriver();

            } else if (browserName.equalsIgnoreCase("firefox")) {
                webDriver = new FirefoxDriver();
            }

            webDriver.manage().window().maximize();
        }catch (Exception exception){

        }
    }

    /***
     * Implementing get webdriver functionality
     * @return
     */
    public static WebDriver getWebDriver() {
        return webDriver;

    }

    /***
     * Implementing goToUrl functionality
     * @param url
     */
    public static void goToUrl(String url){
        webDriver.get(url);

    }

    /***
     * Implementing closeFocusedScreen functionality
     */
    public static void closeFocusedScreen(){
        webDriver.close();

    }

    /***
     * Implementing quitBrowser functionality
     */
    public static void quitBrowser(){
        webDriver.quit();

    }

}
